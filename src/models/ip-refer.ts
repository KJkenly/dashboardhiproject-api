import Knex, * as knex from 'knex';

export class IPReferModel {

  constructor() { }

  select_cln(db: knex) {
    return db('cln');
  }

  select_yearbudget(db:knex){
    return db('yearbudget')
  }

  async getNameWard(db:knex,ward:any){
    let sql:any = `select nameidpm from idpm as c
    where c.idpm =  ${ward}`;
   let data:any = await db.raw(sql);
   return data[0];
  }
//REfer out by ward
async getReferoutByWard(db: Knex, yearbudget: any,ward: any) {
  if (ward=='' || ward == 'null'){
    let data = await db('ipt as o')
    .innerJoin('idpm as c', 'c.idpm', 'o.ward')
    .innerJoin('orfro as r','o.an','r.an')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.dchtype','=','4')
    .countDistinct('o.an as value')
    .select('c.nameidpm as name')
    .groupBy('o.ward');
    return data;
  }else{
    let data = await db('ipt as o')
    .innerJoin('idpm as c', 'c.idpm', 'o.ward')
    .innerJoin('orfro as r','o.an','r.an')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.ward',ward)
    .andWhere('o.dchtype','=','4')
    .countDistinct('o.an as value')
    .select('c.nameidpm as name')
    .groupBy('o.ward');
    return data;
  }

}

//REfer in by ward
async getReferinByWard(db: Knex, yearbudget: any,ward: any) {

  if (ward=='' || ward == 'null'){
    let data = await db('ipt as o')
    .innerJoin('idpm as c', 'c.idpm', 'o.ward')
    .innerJoin('orfri as r','o.vn','r.vn')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .count('o.vn as value')
    .select('c.nameidpm as name')
    .groupBy('o.idpm');
    return data;
  }else{
    let data = await db('ipt as o')
    .innerJoin('idpm as c', 'c.idpm', 'o.ward')
    .innerJoin('orfri as r','o.vn','r.vn')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.ward',ward)
    .count('o.an as value')
    .select('c.nameward as name')
    .groupBy('o.idpm');
    return data;
  }

}

// REfer out by Age
async getReferoutByAge(db: Knex, yearbudget: any,ward: any) {
  if (ward =='' || ward == 'null'){
      let data = await db('ipt as o')
      .innerJoin('pt', 'pt.hn', 'o.hn')
      .innerJoin('orfro as r','o.an','r.an')
      .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
      .andWhere('o.dchtype','=','4')
      .countDistinct('o.an as value')
      .select(db.raw('count(distinct case pt.male when "1" then o.vn end) as male'))
      .select(db.raw('count(distinct case pt.male when "2" then o.vn end) as female'))
      .select(db.raw('(case when timestampdiff(year,pt.brthdate,o.dchdate) between 0 and 9 then "0-9"' +
          ' when timestampdiff(year,pt.brthdate,o.dchdate) between 10 and 19 then "10-19"' +
          ' when timestampdiff(year,pt.brthdate,o.dchdate) between 20 and 29 then "20-29"' +
          ' when timestampdiff(year,pt.brthdate,o.dchdate) between 30 and 39 then "30-39"' +
          ' when timestampdiff(year,pt.brthdate,o.dchdate) between 40 and 49 then "40-49"' +
          ' when timestampdiff(year,pt.brthdate,o.dchdate) between 50 and 59 then "50-59"' +
          ' when timestampdiff(year,pt.brthdate,o.dchdate) between 60 and 69 then "60-69"' +
          ' when timestampdiff(year,pt.brthdate,o.dchdate) between 70 and 79 then "70-79"' +
          ' when timestampdiff(year,pt.brthdate,o.dchdate) between 80 and 89 then "80-89"' +
          ' when timestampdiff(year,pt.brthdate,o.dchdate) between 90 and 99 then "90-99"' +
          ' when timestampdiff(year,pt.brthdate,o.dchdate) >= 100 then "99 up" else "NA" end) as name'))
      .groupBy('name');
    return data;
  }else{
    let data = await db('ipt as o')
    .innerJoin('pt', 'pt.hn', 'o.hn')
    .innerJoin('orfro as r','o.an','r.an')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.dchtype','=','4')
    .andWhere('o.ward',ward)
    .countDistinct('o.an as value')
    .select(db.raw('count(distinct case pt.male when "1" then o.vn end) as male'))
    .select(db.raw('count(distinct case pt.male when "2" then o.vn end) as female'))
    .select(db.raw('(case when timestampdiff(year,pt.brthdate,o.dchdate) between 0 and 9 then "0-9"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 10 and 19 then "10-19"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 20 and 29 then "20-29"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 30 and 39 then "30-39"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 40 and 49 then "40-49"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 50 and 59 then "50-59"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 60 and 69 then "60-69"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 70 and 79 then "70-79"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 80 and 89 then "80-89"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 90 and 99 then "90-99"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) >= 100 then "99 up" else "NA" end) as name'))
    .groupBy('name');
  return data;    
  }

}

//Refer in by age
async getReferinByAge(db: Knex, yearbudget: any,ward: any) {
  if (ward =='' || ward == 'null'){
    let data = await db('ipt as o')
    .innerJoin('pt', 'pt.hn', 'o.hn')
    .innerJoin('orfri as r','o.vn','r.vn')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .count('o.an as value')
    .select(db.raw('count(case pt.male when "1" then o.vn end) as male'))
    .select(db.raw('count(case pt.male when "2" then o.vn end) as female'))
    .select(db.raw('(case when timestampdiff(year,pt.brthdate,o.dchdate) between 0 and 9 then "0-9"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 10 and 19 then "10-19"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 20 and 29 then "20-29"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 30 and 39 then "30-39"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 40 and 49 then "40-49"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 50 and 59 then "50-59"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 60 and 69 then "60-69"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 70 and 79 then "70-79"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 80 and 89 then "80-89"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) between 90 and 99 then "90-99"' +
        ' when timestampdiff(year,pt.brthdate,o.dchdate) >= 100 then "99 up" else "NA" end) as name'))
    .groupBy('name');
  return data;
}else{
  let data = await db('ipt as o')
  .innerJoin('pt', 'pt.hn', 'o.hn')
  .innerJoin('orfri as r','o.an','r.an')
  .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
  .andWhere('o.ward',ward)
  .count('o.an as value')
  .select(db.raw('count(case pt.male when "1" then o.vn end) as male'))
  .select(db.raw('count(case pt.male when "2" then o.vn end) as female'))
  .select(db.raw('(case when timestampdiff(year,pt.brthdate,o.dchdate) between 0 and 9 then "0-9"' +
      ' when timestampdiff(year,pt.brthdate,o.dchdate) between 10 and 19 then "10-19"' +
      ' when timestampdiff(year,pt.brthdate,o.dchdate) between 20 and 29 then "20-29"' +
      ' when timestampdiff(year,pt.brthdate,o.dchdate) between 30 and 39 then "30-39"' +
      ' when timestampdiff(year,pt.brthdate,o.dchdate) between 40 and 49 then "40-49"' +
      ' when timestampdiff(year,pt.brthdate,o.dchdate) between 50 and 59 then "50-59"' +
      ' when timestampdiff(year,pt.brthdate,o.dchdate) between 60 and 69 then "60-69"' +
      ' when timestampdiff(year,pt.brthdate,o.dchdate) between 70 and 79 then "70-79"' +
      ' when timestampdiff(year,pt.brthdate,o.dchdate) between 80 and 89 then "80-89"' +
      ' when timestampdiff(year,pt.brthdate,o.dchdate) between 90 and 99 then "90-99"' +
      ' when timestampdiff(year,pt.brthdate,o.dchdate) >= 100 then "99 up" else "NA" end) as name'))
  .groupBy('name');
return data;    
}

}
//Refer out by Gender OK
async getReferoutByGender(db: Knex, yearbudget: any,ward: any) {
  if(ward=='' || ward =='null'){
    let data = await db('ipt as o')
    .innerJoin('orfro as r','o.an','r.an')
    .innerJoin('pt', 'pt.hn', 'o.hn')
    .innerJoin('male', 'pt.male', 'male.male')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.dchtype','=','4')
    .countDistinct('o.an as value')
    .select('male.namemale as name')
    .groupBy('pt.male');
  return data; 
  }else{
    let data = await db('ipt as o')
    .innerJoin('orfro as r','o.an','r.an')
    .innerJoin('pt', 'pt.hn', 'o.hn')
    .innerJoin('male', 'pt.male', 'male.male')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.dchtype','=','4')
    .andWhere('o.ward',ward)
    .countDistinct('o.an as value')
    .select('male.namemale as name')
    .groupBy('pt.male');
  return data; 
  }
}


//Refer in by Gender OK
async getReferinByGender(db: Knex, yearbudget: any,ward: any) {
  if(ward=='' || ward =='null'){
    let data = await db('ipt as o')
    .innerJoin('orfri as r','o.an','r.an')
    .innerJoin('pt', 'pt.hn', 'o.hn')
    .innerJoin('male', 'pt.male', 'male.male')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .count('o.an as value')
    .select('male.namemale as name')
    .groupBy('pt.male');
  return data; 
  }else{
    let data = await db('ipt as o')
    .innerJoin('orfri as r','o.an','r.an')
    .innerJoin('pt', 'pt.hn', 'o.hn')
    .innerJoin('male', 'pt.male', 'male.male')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.ward',ward)
    .count('o.vn as value')
    .select('male.namemale as name')
    .groupBy('pt.male');
  return data; 
  }
}
//Refer out by Month OK
async getReferoutByMonth(db: Knex, yearbudget: any,ward: any) {
  if(ward =='' || ward =='null'){
    let data = await db('ipt as o')
    .innerJoin('orfro as r','o.an','r.an')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.dchtype','=','4')
    .countDistinct('o.an as value')
    .select(db.raw('month(o.dchdate) as name'))
    .orderBy('o.dchdate')
    .groupBy('name');

    return data;
  }else{
    let data = await db('ipt as o')
    .innerJoin('orfro as r','o.an','r.an')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.dchtype','=','4')
    .andWhere('o.ward',ward)
    .countDistinct('o.an as value')
    .select(db.raw('month(o.dchdate) as name'))
    .orderBy('o.dchdate')
    .groupBy('name');
    return data;
  }

}

//Refer in by Month
async getReferinByMonth(db: Knex, yearbudget: any,ward: any) {
  if(ward =='' || ward =='null'){
    let data = await db('ipt as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .count('o.vn as value')
    .select(db.raw('month(o.dchdate) as name'))
    .orderBy('o.dchdate')
    .groupBy('name');
    return data;
  }else{
    let data = await db('ipt as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.ward',ward)
    .count('o.vn as value')
    .select(db.raw('month(o.dchdate) as name'))
    .orderBy('o.dchdate')
    .groupBy('name');
    return data;
  }

}

//Refer out by Pttype OK
async getReferoutByPttype(db: Knex, yearbudget: any,ward: any) {
  if(ward=='' || ward=='null'){
    let data = await db('ipt as o')
    .innerJoin('orfro as r','o.an','r.an')
    .innerJoin('pttype as p', 'p.pttype', 'o.pttype')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.dchtype','=','4')
    .countDistinct('o.an as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;   

  }else{
    let data = await db('ipt as o')
    .innerJoin('orfro as r','o.an','r.an')
    .innerJoin('pttype as p', 'p.pttype', 'o.pttype')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.dchtype','=','4')
    .andWhere('o.ward',ward)
    .countDistinct('o.an as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }
}

//Refer in by Pttype
async getReferinByPttype(db: Knex, yearbudget: any,ward: any) {
  if(ward=='' || ward=='null'){
    let data = await db('ipt as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p', 'p.pttype', 'o.pttype')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.dchtype','=','4')
    .count('o.vn as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }else{
    let data = await db('ipt as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p', 'p.pttype', 'o.pttype')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ? ', [yearbudget]))
    .andWhere('o.ward',ward)
    .count('o.vn as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }
}

//Refer out by Incoth month
async getReferout_IncothByMonth(db: Knex, yearbudget: any,ward: any) {
  if(ward =='' || ward=='null'){
    let data = await db('ipt as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('idpm as c','c.idpm','o.ward')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1)= ? ', [yearbudget]))   
    .andWhere('o.dchtype','=','4')       
    .select(db.raw('month(o.dchdate) as name'))
    .countDistinct('o.an as value')
    .sum('i.rcptamt as total')
    .groupBy('name')
    .orderBy('o.dchdate')
    return data;
  }else{
    let data = await db('ipt as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('idpm as c','c.idpm','o.ward')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1)= ? ', [yearbudget]))   
    .andWhere('o.dchtype','=','4')  
    .andWhere('o.ward',ward)     
    .select(db.raw('month(o.dchdate) as name'))
    .countDistinct('o.an as value')
    .sum('i.rcptamt as total')
    .groupBy('name')
    .orderBy('o.dchdate')
    return data;
  }
}

//Refer in by Incoth month
async getReferin_IncothByMonth(db: Knex, yearbudget: any,ward: any) {
  if(ward =='' || ward=='null'){
    let data = await db('ipt as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('idpm as c','c.ipdm','o.ward')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1)= ? ', [yearbudget]))        
    .select(db.raw('month(o.dchdate) as name'))
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('name')
    .orderBy('o.dchdate')
    return data;
  }else{
    let data = await db('ipt as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('idpm as c','c.ipdm','o.ward')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1)= ? ', [yearbudget]))     
    .andWhere('o.ward',ward)   
    .select(db.raw('month(o.dchdate) as name'))
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('name')
    .orderBy('o.dchdate')
    return data;
  }
}

//สรุปค่ารักษา + total visit
async getReferout_IncothByMonthPttype_Inscl(db: Knex, yearbudget: any,ward: any) {
  if(ward=='' || ward=='null'){
    let data = await db('ipt as o')
    .innerJoin('orfro as r','o.an','r.an')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('idpm as c','c.idpm','o.ward')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1)= ? ', [yearbudget]))   
    .andWhere('o.dchtype','=','4')         
    .select('p.inscl as name')
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }else {
    let data = await db('ipt as o')
    .innerJoin('orfro as r','o.an','r.an')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('idpm as c','c.idpm','o.ward')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1)= ? ', [yearbudget]))   
    .andWhere('o.dchtype','=','4')  
    .andWhere('o.ward',ward)       
    .select('p.inscl as name')
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }

}

//สรุปค่ารักษา + total visit
async getReferin_IncothByMonthPttype_Inscl(db: Knex, yearbudget: any,ward: any) {
  if(ward=='' || ward=='null'){
    let data = await db('ipt as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('idpm as c','c.idpm','o.ward')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1)= ? ', [yearbudget]))            
    .select('p.inscl as name')
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }else {
    let data = await db('ipt as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('idpm as c','c.idpm','o.ward')
    .where(db.raw('if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1)= ? ', [yearbudget]))          
    .andWhere('o.ward',ward)
    .select('p.inscl as name')
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }

}
//หาค่า max Refer out ในการแสดงกราฟ
async getMaxReferoutbyMonth(db: Knex, yearbudget: any,ward: any){
  if(ward =='' || ward =='null'){
    let sql:any = `SELECT name,max(value) as value,max(value) / length(max(value)) as strinter  from (
      SELECT month(o.dchdate) as name,count(o.an) as value from ipt as o 
      inner join orfro as r on o.an = r.an and o.dchtype = 4 
      where if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ?
      GROUP BY name
      order by o.dchdate ) as x`
      let data:any = await db.raw(sql,[yearbudget]);
      return data[0];
  }else{
    let sql:any = `SELECT name,max(value) as value,max(value) / length(max(value)) as strinter  from (
      SELECT month(o.dchdate) as name,count(o.an) as value from ipt as o 
      inner join orfro as r on o.an = r.an and o.dchtype = 4 
      where o.ward = ? and if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ?
      GROUP BY name
      order by o.dchdate ) as x`
      let data:any = await db.raw(sql,[ward,yearbudget]);
      return data[0];
  }

}

//หาค่า max Refer in ในการแสดงกราฟ
async getMaxReferinbyMonth(db: Knex, yearbudget: any,ward: any){
  if(ward =='' || ward =='null'){
    let sql:any = `SELECT name,max(value) as value,max(value) / length(max(value)) as strinter  from (
      SELECT month(o.dchdate) as name,count(o.vn) as value from ipt as o 
      inner join orfri as r on o.vn = r.vn 
      where if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ?
      GROUP BY name
      order by o.dchdate ) as x`
      let data:any = await db.raw(sql,[yearbudget]);
      return data[0];
  }else{
    let sql:any = `SELECT name,max(value) as value,max(value) / length(max(value)) as strinter  from (
      SELECT month(o.dchdate) as name,count(o.vn) as value from ipt as o 
      inner join orfri as r on o.vn = r.vn 
      where o.ward = ? and if(month(o.dchdate) < 10,year(o.dchdate),year(o.dchdate)+1) = ?
      GROUP BY name
      order by o.dchdate ) as x`
      let data:any = await db.raw(sql,[ward,yearbudget]);
      return data[0];
  }

}
//20 ลำดับโรค Refer out
async getDx20Referout(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `SELECT x.icd10 as code,i.icd10name as name,count(x.icd10) as total from ipt as o 
    INNER JOIN orfro as r on o.an = r.an and o.dchtype = 4
    INNER JOIN iptdx as x on o.an = x.an and x.itemno = 1
    INNER JOIN icd101 as i on x.icd10 = i.icd10
    where date(o.dchdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY x.icd10 ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT x.icd10 as code,i.icd10name as name,count(x.icd10) as total from ipt as o 
    INNER JOIN orfro as r on o.an = r.an and o.dchtype = 4
    INNER JOIN iptdx as x on o.an = x.an and x.itemno = 1
    INNER JOIN icd101 as i on x.icd10 = i.icd10
    where o.ward = '${ward}' and date(o.dchdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY x.icd10 ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับโรค Refer in
async getDx20Referin(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `SELECT x.icd10 as code,i.icd10name as name,count(x.icd10) as total from ipt as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN iptdx as x on o.an = x.an and x.itemno = 1
    INNER JOIN icd101 as i on x.icd10 = i.icd10
    where date(o.dchdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY x.icd10 ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT x.icd10 as code,i.icd10name as name,count(x.icd10) as total from ipt as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN iptdx as x on o.an = x.an and x.itemno = 1
    INNER JOIN icd101 as i on x.icd10 = i.icd10
    where o.ward = '${ward}' and date(o.dchdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY x.icd10 ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับคลินิก Refer out
async getCLN20Referout(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `SELECT ward as code,nameidpm as name,count(ward) as total from (
      SELECT  DISTINCT o.an,o.ward,c.nameidpm from ipt as o 
          INNER JOIN orfro as r on o.an = r.an and o.dchtype = 4
          INNER JOIN idpm as c on o.ward = c.idpm
          where  date(o.dchdate) BETWEEN  '${date_start}' and '${date_end}')
      as x GROUP BY x.ward order by total desc limit 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT ward as code,nameidpm as name,count(ward) as total from (
      SELECT  DISTINCT o.an,o.ward,c.nameidpm from ipt as o 
          INNER JOIN orfro as r on o.an = r.an and o.dchtype = 4
          INNER JOIN idpm as c on o.ward = c.idpm
          where o.ward = '${ward}'and date(o.dchdate) BETWEEN  '${date_start}' and '${date_end}')
      as x GROUP BY x.ward order by total desc limit 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}
//20 ลำดับคลินิก Refer in
async getCLN20Referin(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `SELECT o.ward as code,c.nameidpm as name,count(o.ward) as total from ipt as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN idpm as c on o.ward = c.idpm
    where date(o.dchdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY o.ward ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT o.ward as code,c.nameidpm as name,count(o.ward) as total from ipt as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN idpm as c on o.ward = c.idpm
    where o.ward = '${ward}' and date(o.dchdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY o.ward ORDER BY total desc  LIMIT 20`;

   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับสาเหตุการ  Refer out
async getRfrcs20Referout(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `SELECT rfrcs as code,namerfrcs as name ,count(rfrcs) as total from (
      SELECT  o.an, r.rfrcs,if(h.namerfrcs is null,'ไม่ระบุสาเหตุ',h.namerfrcs) as namerfrcs  from ipt as o 
      INNER JOIN orfro as r on o.an = r.an and o.dchtype = 4
      inner JOIN rfrcs as h on r.rfrcs = h.rfrcs
      where  date(o.dchdate) BETWEEN '${date_start}' and '${date_end}'  GROUP BY o.an  ORDER BY o.an,r.vstdate,r.vsttime desc   ) as x GROUP BY namerfrcs order by total desc LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT rfrcs as code,namerfrcs as name ,count(rfrcs) as total from (
      SELECT  o.an, r.rfrcs,if(h.namerfrcs is null,'ไม่ระบุสาเหตุ',h.namerfrcs) as namerfrcs  from ipt as o 
      INNER JOIN orfro as r on o.an = r.an and o.dchtype = 4
      inner JOIN rfrcs as h on r.rfrcs = h.rfrcs
      where o.ward = '${ward}' and date(o.dchdate) BETWEEN '${date_start}' and '${date_end}'  GROUP BY o.an  ORDER BY o.an,r.vstdate,r.vsttime desc   ) as x GROUP BY namerfrcs order by total desc LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับสาเหตุการ  Refer in
async getRfrcs20Referin(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `SELECT rfrcs as code,namerfrcs as name ,sum(total) as total from (
      SELECT r.rfrcs,if(h.namerfrcs is null,'ไม่ระบุสาเหตุ',h.namerfrcs) as namerfrcs  ,count(r.rfrcs) as total from ipt as o 
      INNER JOIN orfri as r on o.vn = r.vn 
      left JOIN rfrcs as h on r.rfrcs = h.rfrcs
      where date(o.dchdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrcs ORDER BY total desc  LIMIT 20 ) as x GROUP BY namerfrcs order by total desc`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT rfrcs as code,namerfrcs as name ,sum(total) as total from (
      SELECT r.rfrcs,if(h.namerfrcs is null,'ไม่ระบุสาเหตุ',h.namerfrcs) as namerfrcs  ,count(r.rfrcs) as total from ipt as o 
      INNER JOIN orfri as r on o.vn = r.vn 
      left JOIN rfrcs as h on r.rfrcs = h.rfrcs
      where o.ward = '${ward}' and date(o.dchdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrcs ORDER BY total desc  LIMIT 20 ) as x GROUP BY namerfrcs order by total desc`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับสาเหตุการ  Refer out 
async getHcode20Referout(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `SELECT x.rfrlct as code,namehosp as name,count(rfrlct) as total from (
      SELECT r.rfrlct,h.namehosp,o.an from ipt as o 
          INNER JOIN orfro as r on o.an = r.an and o.dchtype = 4
          INNER JOIN hospcode as h on r.rfrlct = h.off_id
          where date(o.dchdate) BETWEEN '${date_start}' and '${date_end}'  GROUP BY o.an  ORDER BY o.an,r.vstdate,r.vsttime desc) as x GROUP BY rfrlct ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT x.rfrlct as code,namehosp as name,count(rfrlct) as total from (
      SELECT r.rfrlct,h.namehosp,o.an from ipt as o 
          INNER JOIN orfro as r on o.an = r.an and o.dchtype = 4
          INNER JOIN hospcode as h on r.rfrlct = h.off_id
          where o.ward  = '${ward}' and date(o.dchdate) BETWEEN '${date_start}' and '${date_end}'  GROUP BY o.an  ORDER BY o.an,r.vstdate,r.vsttime desc) as x GROUP BY rfrlct ORDER BY total desc  LIMIT 20`;
   
    let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับสาเหตุการ  Refer in
async getHcode20Referin(db: Knex, date_start:any,date_end: any,ward: any){
  if (ward=='' || ward == 'null'){     
    let sql:any = `SELECT r.rfrlct as code,h.namehosp as name,count(r.rfrlct) as total from ipt as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN hospcode as h on r.rfrlct = h.off_id
    where date(o.dchdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrlct ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT r.rfrlct as code,h.namehosp as name,count(r.rfrlct) as total from ipt as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN hospcode as h on r.rfrlct = h.off_id
    where o.ward = '${ward}' and date(o.dchdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrlct ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}
}