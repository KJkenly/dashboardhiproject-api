import Knex, * as knex from 'knex';

export class StmOpStpModel {

  constructor() { }

  select(db: knex) {
    return db('stm_op_stp');
  }

  async select_debt_account(db: knex) {
    let sql:any  =  `SELECT * from debt_account as s
        WHERE s.debt_type = 'OPD' `;
     console.log('sql:',sql);   
     let data: any =  await db.raw(sql)
    return data[0];
  }
  async select_debt_account_hi(db: knex) {
    let sql:any  =  `SELECT dept_name as debt_account_name,op_code as debt_account_code from dept_code as s `;
     console.log('sql:',sql);   
     let data: any =  await db.raw(sql)
    return data[0];
  }
  async stm_countvn(db:Knex,accType:any,startDate:any,endDate:any){
    if (accType=='' || accType == 'null'){
        let sql:any =` SELECT 
        COUNT(DISTINCT d.vn) AS all_op 
        FROM debit_op AS d 
        WHERE visit_date BETWEEN ? AND ? `
        let data: any = await db.raw(sql, [startDate, endDate])
        return data[0];  
    }
    else {
        let sql:any =` SELECT 
        COUNT(DISTINCT d.vn) AS all_op 
        FROM debit_op AS d 
        WHERE d.acc_code = ?
        AND visit_date BETWEEN ? AND ? `
        let data: any = await db.raw(sql, [accType, startDate, endDate])
        return data[0];  
    }
  }

  async select_stm_op_stp(db: Knex,repno: number) {
    let sql:any  =  `SELECT s.*,if(length(substr(s.pid,5))= 9,concat(substr(s.pid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,(s.charge - s.total_summary) as diff from stm_op_stp as s
        WHERE s.repno = ? `;
     console.log('sql:',sql);
    let data: any = await db.raw(sql, [repno])
    
    return data[0];
         
  } 

  async select_stm_op_stp_stmno(db: Knex,stm_no: number) {
    let sql:any  =  `SELECT s.*,if(length(substr(s.pid,5))= 9,concat(substr(s.pid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,(s.charge - s.total_summary) as diff from stm_op_stp as s
        WHERE s.stm_no = ? order by repno asc`;
     console.log('sql:',sql);
    let data: any = await db.raw(sql, [stm_no])
    
    return data[0];
         
  } 
  async opstpnull(db: Knex, accType: any, startDate: any, endDate: any) {
    if (accType=='' || accType == 'null'){ 
        let sql: any = `SELECT * from (
            SELECT d.hn,
                d.vn,
                if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
                d.fullname,
                d.pttype_code,
                 d.pttype_name,
                d.acc_code,
                (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
                DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
                 DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
                 d.charge AS charge ,
                 d.paid AS paid, d.debt AS debt ,
                r.repno AS repno,
                r.errorcode AS error_code ,
                 v.code_name AS error_name ,
                 v.remarkdata AS remark_data ,
                 r.total_paid AS total_paid ,
                 s.total_summary,s.projcode,
                 CASE WHEN r.total_paid IS NOT NULL THEN (d.debt-r.total_paid) 
                ELSE NULL END AS rep_diff ,
                CASE WHEN (d.debt-r.total_paid) <= 0 THEN 0 ELSE (d.debt-r.total_paid) END AS rest_debt 
                FROM debit_op AS d 
                    LEFT JOIN (SELECT * FROM stm_op_stp GROUP BY HN+date_serv) AS s  
                ON (d.hn = s.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(s.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(s.date_serv,'%H:%i'))) 
                LEFT JOIN rep_op_stp AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
                LEFT JOIN l_validatedata  AS v on r.errorcode = v.code_id 
            WHERE visit_date BETWEEN ? AND ?
            GROUP BY vn 
            ORDER BY visit_date ) as x where repno IS NULL or error_code != '-'`;
        let data: any = await db.raw(sql, [startDate, endDate])
        return data[0];
    }else {
        let sql: any = `SELECT * from (
            SELECT d.hn,
                d.vn,
                if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
                d.fullname,
                d.pttype_code,
                 d.pttype_name,
                d.acc_code,
                (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
                DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
                 DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
                 d.charge AS charge ,
                 d.paid AS paid, d.debt AS debt ,
                r.repno AS repno,
                r.errorcode AS error_code ,
                 v.code_name AS error_name ,
                 v.remarkdata AS remark_data ,
                 r.total_paid AS total_paid ,
                 s.total_summary,s.projcode,
                 CASE WHEN r.total_paid IS NOT NULL THEN (d.debt-r.total_paid) 
                ELSE NULL END AS rep_diff ,
                CASE WHEN (d.debt-r.total_paid) <= 0 THEN 0 ELSE (d.debt-r.total_paid) END AS rest_debt 
                FROM debit_op AS d 
                    LEFT JOIN (SELECT * FROM stm_op_stp GROUP BY HN+date_serv) AS s  
                ON (d.hn = s.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(s.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(s.date_serv,'%H:%i'))) 
                LEFT JOIN rep_op_stp AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
                LEFT JOIN l_validatedata  AS v on r.errorcode = v.code_id 
            WHERE acc_code = ? and visit_date BETWEEN ? AND ?
            GROUP BY vn 
            ORDER BY visit_date ) as x where repno IS NULL or error_code != '-'`;
        let data: any = await db.raw(sql, [accType, startDate, endDate])
        return data[0];
    }

}

async opstpnotnull(db: Knex, accType: any, startDate: any, endDate: any) {
    if (accType=='' || accType == 'null'){ 
        let sql: any = `SELECT * from (
            SELECT d.hn,
                d.vn,
                if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
                d.fullname,
                d.pttype_code,
                 d.pttype_name,
                d.acc_code,
                (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
                DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
                 DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
                 d.charge AS charge ,
                 d.paid AS paid, d.debt AS debt ,
                r.repno AS repno,
                r.errorcode AS error_code ,
                 v.code_name AS error_name ,
                 v.remarkdata AS remark_data ,
                 r.total_paid AS total_paid ,
                 s.total_summary,s.projcode,
                 CASE WHEN r.total_paid IS NOT NULL THEN (d.debt-r.total_paid) 
                ELSE NULL END AS rep_diff ,
                CASE WHEN (d.debt-r.total_paid) <= 0 THEN 0 ELSE (d.debt-r.total_paid) END AS rest_debt 
                FROM debit_op AS d 
                    LEFT JOIN (SELECT * FROM stm_op_stp GROUP BY HN+date_serv) AS s  
                ON (d.hn = s.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(s.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(s.date_serv,'%H:%i'))) 
                LEFT JOIN rep_op_stp AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
                LEFT JOIN l_validatedata  AS v on r.errorcode = v.code_id 
            WHERE  visit_date BETWEEN ? AND ? 
            GROUP BY vn 
            ORDER BY visit_date ) as x where repno IS not NULL and error_code = '-'`;
        let data: any = await db.raw(sql, [startDate, endDate])
        return data[0];        
    }else {
        let sql: any = `SELECT * from (
            SELECT d.hn,
                d.vn,
                if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
                d.fullname,
                d.pttype_code,
                 d.pttype_name,
                d.acc_code,
                (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
                DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
                 DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
                 d.charge AS charge ,
                 d.paid AS paid, d.debt AS debt ,
                r.repno AS repno,
                r.errorcode AS error_code ,
                 v.code_name AS error_name ,
                 v.remarkdata AS remark_data ,
                 r.total_paid AS total_paid ,
                 s.total_summary,s.projcode,
                 CASE WHEN r.total_paid IS NOT NULL THEN (d.debt-r.total_paid) 
                ELSE NULL END AS rep_diff ,
                CASE WHEN (d.debt-r.total_paid) <= 0 THEN 0 ELSE (d.debt-r.total_paid) END AS rest_debt 
                FROM debit_op AS d 
                    LEFT JOIN (SELECT * FROM stm_op_stp GROUP BY HN+date_serv) AS s  
                ON (d.hn = s.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(s.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(s.date_serv,'%H:%i'))) 
                LEFT JOIN rep_op_stp AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
                LEFT JOIN l_validatedata  AS v on r.errorcode = v.code_id 
            WHERE acc_code = ? and visit_date BETWEEN ? AND ? 
            GROUP BY vn 
            ORDER BY visit_date ) as x where repno IS not NULL and error_code = '-'`;
        let data: any = await db.raw(sql, [accType, startDate, endDate])
        return data[0];
    }

}

async opstpaccnull(db: Knex, accType: any, startDate: any, endDate: any) {
    if (accType=='' || accType == 'null'){ 
        let sql: any = `SELECT  COUNT(vn)  AS count_vn ,
        SUM(debt) AS  sum_debt from (
    SELECT d.hn,
        d.vn,
        if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
        d.fullname,
        d.pttype_code,
         d.pttype_name,
        d.acc_code,
        (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
        DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
         DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
         d.charge AS charge ,
         d.paid AS paid, d.debt AS debt ,
        r.repno AS repno,
        r.errorcode AS error_code ,
         v.code_name AS error_name ,
         v.remarkdata AS remark_data ,
         r.total_paid AS total_paid ,
         s.total_summary,
         CASE WHEN r.total_paid IS NOT NULL THEN (d.debt-r.total_paid) 
        ELSE NULL END AS rep_diff ,
        CASE WHEN (d.debt-r.total_paid) <= 0 THEN 0 ELSE (d.debt-r.total_paid) END AS rest_debt 
        FROM debit_op AS d 
            LEFT JOIN (SELECT * FROM stm_op_stp GROUP BY HN+date_serv) AS s  
        ON (d.hn = s.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(s.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(s.date_serv,'%H:%i'))) 
        LEFT JOIN rep_op_stp AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
        LEFT JOIN l_validatedata  AS v on r.errorcode = v.code_id 
    WHERE visit_date BETWEEN ? AND ? 
    GROUP BY vn 
    ORDER BY visit_date ) as x where repno IS NULL or error_code != '-' `;
        let data: any = await db.raw(sql, [startDate, endDate])
        return data[0];       
    }else {
        let sql: any = `SELECT  COUNT(vn)  AS count_vn ,
        SUM(debt) AS  sum_debt from (
    SELECT d.hn,
        d.vn,
        if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
        d.fullname,
        d.pttype_code,
         d.pttype_name,
        d.acc_code,
        (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
        DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
         DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
         d.charge AS charge ,
         d.paid AS paid, d.debt AS debt ,
        r.repno AS repno,
        r.errorcode AS error_code ,
         v.code_name AS error_name ,
         v.remarkdata AS remark_data ,
         r.total_paid AS total_paid ,
         s.total_summary,
         CASE WHEN r.total_paid IS NOT NULL THEN (d.debt-r.total_paid) 
        ELSE NULL END AS rep_diff ,
        CASE WHEN (d.debt-r.total_paid) <= 0 THEN 0 ELSE (d.debt-r.total_paid) END AS rest_debt 
        FROM debit_op AS d 
            LEFT JOIN (SELECT * FROM stm_op_stp GROUP BY HN+date_serv) AS s  
        ON (d.hn = s.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(s.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(s.date_serv,'%H:%i'))) 
        LEFT JOIN rep_op_stp AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
        LEFT JOIN l_validatedata  AS v on r.errorcode = v.code_id 
    WHERE acc_code = ? and visit_date BETWEEN ? AND ? 
    GROUP BY vn 
    ORDER BY visit_date ) as x where repno IS NULL or error_code != '-' `;
        let data: any = await db.raw(sql, [accType, startDate, endDate])
        return data[0];
    }

}

async opstpaccnotnull(db: Knex, accType: any, startDate: any, endDate: any) {
    if (accType=='' || accType == 'null'){ 
        let sql: any = `SELECT  COUNT(vn) AS 'all_notnullcase' ,
        SUM(debt) AS  'debit_notnull',
        SUM(total_summary) AS 'recieve' ,
        SUM(diff) AS 'sum_diff' , SUM(CASE WHEN diff > 0 THEN diff ELSE 0 END) AS diffgain ,
         SUM(CASE WHEN diff < 0 THEN diff ELSE 0 END) AS diffloss from (
    SELECT d.hn,
        d.vn,
        if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
        d.fullname,
        d.pttype_code,
         d.pttype_name,
        d.acc_code,
        (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
        DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
         DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
         d.charge AS charge ,
         d.paid AS paid, d.debt AS debt ,
        r.repno AS repno,
        r.errorcode AS error_code ,
         v.code_name AS error_name ,
         v.remarkdata AS remark_data ,
         r.total_paid AS total_paid ,
             s.total_summary,
            (d.debt-s.total_summary) AS 'diff',
         CASE WHEN r.total_paid IS NOT NULL THEN (d.debt-r.total_paid) 
        ELSE NULL END AS rep_diff ,
        CASE WHEN (d.debt-r.total_paid) <= 0 THEN 0 ELSE (d.debt-r.total_paid) END AS rest_debt 
        FROM debit_op AS d 
            LEFT JOIN (SELECT * FROM stm_op_stp GROUP BY HN+date_serv) AS s  
        ON (d.hn = s.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(s.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(s.date_serv,'%H:%i'))) 
        LEFT JOIN rep_op_stp AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
        LEFT JOIN l_validatedata  AS v on r.errorcode = v.code_id 
    WHERE visit_date BETWEEN ? AND ? 
    GROUP BY vn 
    ORDER BY visit_date ) as x where repno IS not NULL and error_code = '-'`;
        let data: any = await db.raw(sql, [startDate, endDate])
        return data[0];       
    }else {
        let sql: any = `SELECT  COUNT(vn) AS 'all_notnullcase' ,
        SUM(debt) AS  'debit_notnull',
        SUM(total_summary) AS 'recieve' ,
        SUM(diff) AS 'sum_diff' , SUM(CASE WHEN diff > 0 THEN diff ELSE 0 END) AS diffgain ,
         SUM(CASE WHEN diff < 0 THEN diff ELSE 0 END) AS diffloss from (
    SELECT d.hn,
        d.vn,
        if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
        d.fullname,
        d.pttype_code,
         d.pttype_name,
        d.acc_code,
        (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
        DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
         DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
         d.charge AS charge ,
         d.paid AS paid, d.debt AS debt ,
        r.repno AS repno,
        r.errorcode AS error_code ,
         v.code_name AS error_name ,
         v.remarkdata AS remark_data ,
         r.total_paid AS total_paid ,
             s.total_summary,
            (d.debt-s.total_summary) AS 'diff',
         CASE WHEN r.total_paid IS NOT NULL THEN (d.debt-r.total_paid) 
        ELSE NULL END AS rep_diff ,
        CASE WHEN (d.debt-r.total_paid) <= 0 THEN 0 ELSE (d.debt-r.total_paid) END AS rest_debt 
        FROM debit_op AS d 
            LEFT JOIN (SELECT * FROM stm_op_stp GROUP BY HN+date_serv) AS s  
        ON (d.hn = s.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(s.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(s.date_serv,'%H:%i'))) 
        LEFT JOIN rep_op_stp AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
        LEFT JOIN l_validatedata  AS v on r.errorcode = v.code_id 
    WHERE acc_code = ? and visit_date BETWEEN ? AND ? 
    GROUP BY vn 
    ORDER BY visit_date ) as x where repno IS not NULL and error_code = '-'`;
        let data: any = await db.raw(sql, [accType, startDate, endDate])
        return data[0];
    }

}

async opstpaccbydate(db: Knex, accType: any, startDate: any, endDate: any) {
    if (accType=='' || accType == 'null'){ 
        let sql: any = `SELECT     visit_date
        , COUNT(vn)  AS allcase 
        , SUM(debt) AS  debit 
        , SUM(CASE WHEN repno IS NULL or error_code != '-' THEN 1 ELSE 0 END)  AS nullcase,
        SUM(CASE WHEN repno IS  NULL or error_code != '-' THEN debt ELSE 0 END)  AS nulldebit ,
        SUM(CASE WHEN repno IS NOT NULL and error_code = '-' THEN 1 ELSE 0 END)  AS notnullcase
        ,SUM(CASE WHEN repno IS NOT NULL and error_code = '-' THEN debt ELSE 0 END)  AS notnulldebit,
        SUM(total_summary) AS recieve 
        ,SUM(CASE WHEN repno IS NOT NULL and error_code = '-' THEN debt ELSE 0 END)-SUM(total_summary) AS diff  
    from 
    (SELECT d.hn,
        d.vn,
        if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
        d.fullname,
        d.pttype_code,
         d.pttype_name,
        d.acc_code,
        (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
        DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
         DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
         d.charge AS charge ,
         d.paid AS paid, d.debt AS debt ,
        r.repno AS repno,
        r.errorcode AS error_code ,
         v.code_name AS error_name ,
         v.remarkdata AS remark_data ,
         r.total_paid AS total_paid ,s.total_summary,
         CASE WHEN r.total_paid IS NOT NULL THEN (d.debt-r.total_paid) 
        ELSE NULL END AS rep_diff ,
        CASE WHEN (d.debt-r.total_paid) <= 0 THEN 0 ELSE (d.debt-r.total_paid) END AS rest_debt 
        FROM debit_op AS d 
            LEFT JOIN (SELECT * FROM stm_op_stp GROUP BY HN+date_serv) AS s  
        ON (d.hn = s.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(s.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(s.date_serv,'%H:%i'))) 
        LEFT JOIN rep_op_stp AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
        LEFT JOIN l_validatedata  AS v on r.errorcode = v.code_id 
    WHERE visit_date BETWEEN ? AND ? 
    GROUP BY vn 
    ORDER BY visit_date ) as x GROUP BY visit_date`;
        let data: any = await db.raw(sql, [startDate, endDate])
        return data[0];       
    }else {
        let sql: any = `SELECT     visit_date
        , COUNT(vn)  AS allcase 
        , SUM(debt) AS  debit 
        , SUM(CASE WHEN repno IS NULL or error_code != '-' THEN 1 ELSE 0 END)  AS nullcase,
        SUM(CASE WHEN repno IS  NULL or error_code != '-' THEN debt ELSE 0 END)  AS nulldebit ,
        SUM(CASE WHEN repno IS NOT NULL and error_code = '-' THEN 1 ELSE 0 END)  AS notnullcase
        ,SUM(CASE WHEN repno IS NOT NULL and error_code = '-' THEN debt ELSE 0 END)  AS notnulldebit,
        SUM(total_summary) AS recieve 
        ,SUM(CASE WHEN repno IS NOT NULL and error_code = '-' THEN debt ELSE 0 END)-SUM(total_summary) AS diff  
    from 
    (SELECT d.hn,
        d.vn,
        if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
        d.fullname,
        d.pttype_code,
         d.pttype_name,
        d.acc_code,
        (SELECT a.debt_account_name from debt_account as a where a.debt_account_code = d.acc_code) as acc_name,
        DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
         DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
         d.charge AS charge ,
         d.paid AS paid, d.debt AS debt ,
        r.repno AS repno,
        r.errorcode AS error_code ,
         v.code_name AS error_name ,
         v.remarkdata AS remark_data ,
         r.total_paid AS total_paid ,s.total_summary,
         CASE WHEN r.total_paid IS NOT NULL THEN (d.debt-r.total_paid) 
        ELSE NULL END AS rep_diff ,
        CASE WHEN (d.debt-r.total_paid) <= 0 THEN 0 ELSE (d.debt-r.total_paid) END AS rest_debt 
        FROM debit_op AS d 
            LEFT JOIN (SELECT * FROM stm_op_stp GROUP BY HN+date_serv) AS s  
        ON (d.hn = s.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(s.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(s.date_serv,'%H:%i'))) 
        LEFT JOIN rep_op_stp AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
        LEFT JOIN l_validatedata  AS v on r.errorcode = v.code_id 
    WHERE acc_code = ? and visit_date BETWEEN ? AND ? 
    GROUP BY vn 
    ORDER BY visit_date ) as x GROUP BY visit_date`;
        let data: any = await db.raw(sql, [accType, startDate, endDate])
        return data[0];
    }
 
}

async stm_op_stp_sum(db: Knex, repno: any) {
    let sql: any = `SELECT
    stm.repno,
    COUNT(stm.an) AS "all_case",
    SUM(CAST(stm.charge AS DECIMAL)) AS "debt",
    SUM(CAST(stm.total_summary AS DECIMAL)) AS "receive",
    SUM(stm.diff) AS "sum_diff",
    SUM(CASE WHEN stm.diff > 0 THEN stm.diff ELSE 0 END) AS diffgain,
    SUM(CASE WHEN stm.diff < 0 THEN stm.diff ELSE 0 END) AS diffloss
FROM
    (
    SELECT
        sd.repno,
        sd.an,
        CAST(sd.charge AS DECIMAL) AS charge,
        CAST(sd.total_summary AS DECIMAL) AS total_summary,
        (CAST(sd.charge AS DECIMAL) - CAST(sd.total_summary AS DECIMAL)) AS diff
    FROM
        stm_op_stp sd
    WHERE
        sd.repno = ?
    ORDER BY
        sd.date_dch
    ) stm
GROUP BY
    stm.repno`;
    let data: any = await db.raw(sql, [repno])
    return data[0];
}

async stm_op_stp_sum_stmno(db: Knex, stm_no: any) {
    let sql: any = `SELECT
    stm.stm_no,
    COUNT(stm.an) AS "all_case",
    SUM(CAST(stm.charge AS DECIMAL)) AS "debt",
    SUM(CAST(stm.total_summary AS DECIMAL)) AS "receive",
    SUM(stm.diff) AS "sum_diff",
    SUM(CASE WHEN stm.diff > 0 THEN stm.diff ELSE 0 END) AS diffgain,
    SUM(CASE WHEN stm.diff < 0 THEN stm.diff ELSE 0 END) AS diffloss
FROM
    (
    SELECT
        sd.stm_no,
        sd.an,
        CAST(sd.charge AS DECIMAL) AS charge,
        CAST(sd.total_summary AS DECIMAL) AS total_summary,
        (CAST(sd.charge AS DECIMAL) - CAST(sd.total_summary AS DECIMAL)) AS diff
    FROM
        stm_op_stp sd
    WHERE
        sd.stm_no = ?
    ORDER BY
        sd.date_dch
    ) stm
GROUP BY
    stm.stm_no`;
    let data: any = await db.raw(sql, [stm_no])
    return data[0];
}

async stm_op_stp_detail(db: Knex, repno: any) {
    let sql: any = `SELECT
    stm.*
FROM
    (
    SELECT
         sd.*,if(length(substr(sd.pid,5))= 9,concat(substr(sd.pid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,         
        (CAST(sd.charge AS DECIMAL) - CAST(sd.total_summary AS DECIMAL)) AS diff
    FROM
        stm_op_stp sd
    WHERE
        sd.repno = ?
     ORDER BY
        sd.date_dch
    ) stm`;
let data: any = await db.raw(sql, [repno])
    return data[0];
}
async toperrorcode(db: Knex, accType: any, startDate: any, endDate: any) {
    if (accType=='' || accType == 'null'){ 
        let sql: any = `SELECT error_code,
        error_name,
        count(error_code) as counterror
         from (
                    SELECT d.hn,
                        d.vn,
                        if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
                        d.fullname,
                        d.pttype_code,
                         d.pttype_name,
                        d.acc_code,
                        d.acc_name,
                        DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
                         DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
                         d.charge AS charge ,
                         d.paid AS paid, d.debt AS debt ,
                        r.repno AS repno,
                        r.error_code AS error_code ,
                         v.code_name AS error_name ,
                         v.remarkdata AS remark_data 
                        FROM debit_op AS d 
                        LEFT JOIN rep_op_errorcode AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
                        LEFT JOIN l_validatedata  AS v on r.error_code = v.code_id 
                    WHERE r.pttype = 'STP' and d.visit_date BETWEEN ?
                    AND ?
                     
                    ORDER BY visit_date ) as x where repno IS NULL or error_code != '-' GROUP BY error_code ORDER BY counterror desc limit 20`;
        let data: any = await db.raw(sql, [startDate, endDate])
        return data[0];
    }else{
        let sql: any = `SELECT error_code,
        error_name,
        count(error_code) as counterror
         from (
                    SELECT d.hn,
                        d.vn,
                        if(length(substr(d.cid,5))= 9,concat(substr(d.cid,5),'XXXXX'),'XXXXXXXXXXXXX') as cid,
                        d.fullname,
                        d.pttype_code,
                         d.pttype_name,
                        d.acc_code,
                        d.acc_name,
                        DATE_FORMAT(d.visit_date,'%Y-%m-%d') AS visit_date ,
                         DATE_FORMAT(d.visit_time,'%H:%i') AS visit_time ,
                         d.charge AS charge ,
                         d.paid AS paid, d.debt AS debt ,
                        r.repno AS repno,
                        r.error_code AS error_code ,
                         v.code_name AS error_name ,
                         v.remarkdata AS remark_data 
                        FROM debit_op AS d 
                        LEFT JOIN rep_op_errorcode AS r ON (d.hn = r.hn AND DATE_FORMAT(d.visit_date,'%Y-%m-%d')=DATE_FORMAT(r.date_serv,'%Y-%m-%d') AND (TIME_FORMAT(d.visit_time,'%H:%i') = DATE_FORMAT(r.date_serv,'%H:%i'))) 
                        LEFT JOIN l_validatedata  AS v on r.error_code = v.code_id 
                    WHERE r.pttype = 'STP' and d.acc_code = ? and d.visit_date BETWEEN ?
                    AND ?
                    
                    ORDER BY visit_date ) as x where repno IS NULL or error_code != '-' GROUP BY error_code ORDER BY counterror desc limit 20`;
        let data: any = await db.raw(sql, [accType, startDate, endDate])
        return data[0];
    }

}
}