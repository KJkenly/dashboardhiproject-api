import Knex, * as knex from 'knex';

export class ProductivityerModel {

  constructor() { }

  select(db: knex) {
    return db('productivity_er');
  }

  insert(db: knex,datas: any){
    return db('productivity_er').insert(datas);
  }

  update(db: Knex,datas: any,id: any){
    return db('productivity_er').update(datas).where('id',id);
  }

  delete(db: Knex,id: any){
    return db('productivity_er').delete().where('id',id);
  }

  select_id(db: knex,id: any) {
    return db('productivity_er').where('id',id);
  }

  select_date(db: Knex,datestart: any,dateend: any){
    return db('productivity_er').whereBetween('vstdate',[datestart,dateend])
  }
//รายงานภาระงานอุบัติเหตุ-ฉุกเฉิน
async select_date_proder(db: Knex,datestart: any,dateend: any){
  let sql: any = 
  `SELECT DATE_FORMAT(vstdate,'%Y-%m-%d') as vstdate,
  (select tr1 from productivity_er  t1 where t1.wer = 1 and t1.vstdate = e.vstdate ) as mPTtype1,
  (select tr2 from productivity_er  t2 where t2.wer = 1 and t2.vstdate = e.vstdate ) as mPTtype2, 
  (select tr3 from productivity_er  t3 where t3.wer = 1 and t3.vstdate = e.vstdate ) as mPTtype3, 
  (select tr4 from productivity_er  t4 where t4.wer = 1 and t4.vstdate = e.vstdate ) as mPTtype4,
  (select tr5 from productivity_er  t5 where t5.wer = 1 and t5.vstdate = e.vstdate ) as mPTtype5,
  (select tr1 from productivity_er  t1 where t1.wer = 2 and t1.vstdate = e.vstdate ) as aPTtype1,
  (select tr2 from productivity_er  t2 where t2.wer = 2 and t2.vstdate = e.vstdate ) as aPTtype2, 
  (select tr3 from productivity_er  t3 where t3.wer = 2 and t3.vstdate = e.vstdate ) as aPTtype3, 
  (select tr4 from productivity_er  t4 where t4.wer = 2 and t4.vstdate = e.vstdate ) as aPTtype4,
  (select tr5 from productivity_er  t5 where t5.wer = 2 and t5.vstdate = e.vstdate ) as aPTtype5,
  (select tr1 from productivity_er  t1 where t1.wer = 3 and t1.vstdate = e.vstdate ) as nPTtype1, 
  (select tr2 from productivity_er  t2 where t2.wer = 3 and t2.vstdate = e.vstdate ) as nPTtype2, 
  (select tr3 from productivity_er  t3 where t3.wer = 3 and t3.vstdate = e.vstdate ) as nPTtype3, 
  (select tr4 from productivity_er  t4 where t4.wer = 3 and t4.vstdate = e.vstdate ) as nPTtype4, 
  (select tr5 from productivity_er  t5 where t5.wer = 3 and t5.vstdate = e.vstdate ) as nPTtype5, 
  (select sum(tr1) from productivity_er  t1 where  t1.vstdate = e.vstdate ) as SumWESI1,
  (select sum(tr2) from productivity_er  t2 where  t2.vstdate = e.vstdate ) as SumWESI2,
  (select sum(tr3) from productivity_er  t3 where  t3.vstdate = e.vstdate ) as SumWESI3,
  (select sum(tr4) from productivity_er  t4 where  t4.vstdate = e.vstdate ) as SumWESI4,
  (select sum(tr5) from productivity_er  t5 where  t5.vstdate = e.vstdate ) as SumWESI5,
  (select sum(tr1+tr2+tr3+tr4+tr5) from productivity_er  t5 where  t5.vstdate = e.vstdate ) as SumPTofday,
  (select round(sum(h_nr_32),2) from productivity_er  t1 where  t1.vstdate = e.vstdate ) as x32, 
  (select round(sum(h_nr_25),2) from productivity_er  t2 where  t2.vstdate = e.vstdate ) as x25, 
  (select round(sum(h_nr_1),2) from productivity_er  t3 where  t3.vstdate = e.vstdate ) as x1, 
  (select round(sum(h_nr_05),2) from productivity_er  t4 where  t4.vstdate = e.vstdate ) as x05,
  (select round(sum(h_nr_025),2) from productivity_er  t5 where  t5.vstdate = e.vstdate ) as x025,
  (select round(sum(h_nr_32+h_nr_25+h_nr_1+h_nr_05+h_nr_025),2) from productivity_er  t5 where  t5.vstdate = e.vstdate ) as total,
  (select rn from productivity_er  t1 where t1.wer = 1 and t1.vstdate = e.vstdate ) as rn_m, 
  (select rn from productivity_er  t2 where t2.wer = 2 and t2.vstdate = e.vstdate ) as rn_a,
  (select rn from productivity_er  t3 where t3.wer = 3 and t3.vstdate = e.vstdate ) as rn_n, 
  (select sum(rn) from productivity_er  t3 where t3.vstdate = e.vstdate ) as staffofday,
  (select round((sum(rn)*7)/sum(tr1+tr2+tr3+tr4+tr5),2)  from productivity_er  t3 where t3.vstdate = e.vstdate ) as hppd,
  (select round(sum(h_nr_32+h_nr_25+h_nr_1+h_nr_05+h_nr_025)/sum(tr1+tr2+tr3+tr4+tr5),2) from productivity_er  t3 where t3.vstdate = e.vstdate ) as hr_nrs,
  (select round((sum(tr1+tr2+tr3+tr4+tr5)*sum(h_nr_32+h_nr_25+h_nr_1+h_nr_05+h_nr_025)/sum(tr1+tr2+tr3+tr4+tr5)*1.4)/7,2) from productivity_er  t3 where t3.vstdate = e.vstdate ) as prod_use,
  (select round((sum(h_nr_32+h_nr_25+h_nr_1+h_nr_05+h_nr_025)/sum(tr1+tr2+tr3+tr4+tr5)*100)/((sum(rn)*7)/sum(tr1+tr2+tr3+tr4+tr5)),2) from productivity_er  t3 where t3.vstdate = e.vstdate ) as productivity
   from productivity_er as e where date(e.vstdate) between '${datestart}' and '${dateend}' GROUP BY vstdate` 
  let data:any = await db.raw(sql);
  return data[0];

}

}
