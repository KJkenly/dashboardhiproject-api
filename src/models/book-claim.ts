import Knex, * as knex from 'knex';

export class BookClaimModel {

  constructor() { }

  async selete_raw_bookclaim(db: Knex, date_start:any,date_end: any){
    let sql:any = `SELECT o.vn,a.hn,DATE_FORMAT(o.vstdttm, '%Y-%m-%d')  AS vstdttm,DATE_FORMAT(o.vstdttm, '%H:%i')  AS vsttime,year(o.vstdttm) - year(a.brthdate) as age,a.male,
if ( a.male = 1,'ชาย','หญิง') AS sex,IF(a.ntnlty != 99, IF(a.engfname IS NULL OR a.engfname = '', CONCAT(a.pname, ' ', a.fname, ' ', a.lname), CONCAT(a.engpname, ' ', a.engfname, ' ', a.englname)),CONCAT(a.pname, ' ', a.fname, ' ', a.lname)) AS fullname,
p.namepttype,p.inscl,i.pttype AS insure,
hi7_checkinscl.checkinscl,
CASE WHEN p.inscl = hi7_checkinscl.checkinscl THEN 'true' 
WHEN p.inscl = 'UCS' AND (hi7_checkinscl.checkinscl = 'UCS' OR hi7_checkinscl.checkinscl = 'WEL') 
THEN 'true' WHEN hi7_checkinscl.checkinscl IS NULL OR hi7_checkinscl.checkinscl = '' THEN 'na' ELSE 'false' END AS 'checkby',
i.hospmain,i.hospsub,c.namecln,concat(trim( s.fname ),' ',trim( s.lname )) AS nameregis,
kios_pttype.claimcode,visitqueueid.queue_number AS queue_number,
visitqueueid.queue_priority AS priority_name,a.pop_id AS pop_id,
IF(invoice.transaction_uid IS NOT NULL AND invoice.transaction_uid != '', 'booksuccess', '') AS claim_status,
IF ( hi7_screen.vn IS NOT NULL AND hi7_screen.vn != '', 'screensuccess', '' ) AS screen_status,ovstost,
o.an,ntnlty.icon,TIMESTAMPDIFF(HOUR, o.vstdttm, NOW()) AS hours_difference,
kios_pttype.claimtype FROM ovst AS o 
 LEFT JOIN pt AS a ON a.hn = o.hn 
 LEFT JOIN hi7_user as s ON o.register = s.id 
 LEFT JOIN pttype AS p ON p.pttype = o.pttype 
 LEFT JOIN cln AS c ON c.cln = o.cln 
 LEFT JOIN kios_pttype on kios_pttype.vn = o.vn 
 LEFT JOIN visitqueueid ON visitqueueid.vn = o.vn 
 LEFT JOIN insure AS i ON o.vn = i.vn  
 LEFT JOIN invoice ON invoice.vn = o.vn  
 LEFT JOIN hi7_screen ON hi7_screen.vn = o.vn 
 LEFT JOIN ntnlty ON a.ntnlty = ntnlty.ntnlty 
 LEFT JOIN hi7_checkinscl ON o.vn = hi7_checkinscl.vn 
  WHERE date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' order by o.vn desc`;
    let data:any = await db.raw(sql);
    return data[0];
  }


async bookclaim_all_statusflg(db: Knex, date_start:any,date_end: any){
 
      let sql:any = `SELECT count(vn) as total_all,
count(CASE WHEN claimcode is null THEN '' END) AS claimcode_null,
count(CASE WHEN claimcode <> '' THEN 1 END) AS claimcode_notnull,
count(case when claim_status = '' then '' end) as claimstatus_null,
count(case when claim_status <> '' then '1' end) as claimstatus_notnull,
count(case when screen_status = '' THEN '' end) as screen_null,
count(case when screen_status <> '' then '1' end) as screen_notnull
  from (
SELECT o.vn,a.hn,DATE_FORMAT(o.vstdttm, '%Y-%m-%d')  AS vstdttm,DATE_FORMAT(o.vstdttm, '%H:%i')  AS vsttime,year(o.vstdttm) - year(a.brthdate) as age,a.male,
if ( a.male = 1,'ชาย','หญิง') AS sex,IF(a.ntnlty != 99, IF(a.engfname IS NULL OR a.engfname = '', CONCAT(a.pname, ' ', a.fname, ' ', a.lname), CONCAT(a.engpname, ' ', a.engfname, ' ', a.englname)),CONCAT(a.pname, ' ', a.fname, ' ', a.lname)) AS fullname,
p.namepttype,p.inscl,i.pttype AS insure,
hi7_checkinscl.checkinscl,
CASE WHEN p.inscl = hi7_checkinscl.checkinscl THEN 'true' 
WHEN p.inscl = 'UCS' AND (hi7_checkinscl.checkinscl = 'UCS' OR hi7_checkinscl.checkinscl = 'WEL') 
THEN 'true' WHEN hi7_checkinscl.checkinscl IS NULL OR hi7_checkinscl.checkinscl = '' THEN 'na' ELSE 'false' END AS 'checkby',
i.hospmain,i.hospsub,c.namecln,concat(trim( s.fname ),' ',trim( s.lname )) AS nameregis,
kios_pttype.claimcode,visitqueueid.queue_number AS queue_number,
visitqueueid.queue_priority AS priority_name,a.pop_id AS pop_id,
IF(invoice.transaction_uid IS NOT NULL AND invoice.transaction_uid != '', 'booksuccess', '') AS claim_status,
IF ( hi7_screen.vn IS NOT NULL AND hi7_screen.vn != '', 'screensuccess', '' ) AS screen_status,ovstost,
o.an,ntnlty.icon,TIMESTAMPDIFF(HOUR, o.vstdttm, NOW()) AS hours_difference,
kios_pttype.claimtype FROM ovst AS o 
 LEFT JOIN pt AS a ON a.hn = o.hn 
 LEFT JOIN hi7_user as s ON o.register = s.id 
 LEFT JOIN pttype AS p ON p.pttype = o.pttype 
 LEFT JOIN cln AS c ON c.cln = o.cln 
 LEFT JOIN kios_pttype on kios_pttype.vn = o.vn 
 LEFT JOIN visitqueueid ON visitqueueid.vn = o.vn 
 LEFT JOIN insure AS i ON o.vn = i.vn  
 LEFT JOIN invoice ON invoice.vn = o.vn  
 LEFT JOIN hi7_screen ON hi7_screen.vn = o.vn 
 LEFT JOIN ntnlty ON a.ntnlty = ntnlty.ntnlty 
 LEFT JOIN hi7_checkinscl ON o.vn = hi7_checkinscl.vn 
  WHERE date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' order by o.vn desc ) as x`;
     let data:any = await db.raw(sql);
     return data[0];
     
  }

async check_charge(db: Knex, vn: any){
 
    let sql:any = `select c.costcenter,c.namecost,sum(i.rcptamt) as charge from hi.incoth as i
INNER JOIN hi.income as c on i.income = c.costcenter
 where i.vn = ${vn} GROUP BY i.income`;
   let data:any = await db.raw(sql);
   return data[0];
   
}


}