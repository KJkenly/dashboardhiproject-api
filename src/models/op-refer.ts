import Knex, * as knex from 'knex';

export class OPReferModel {

  constructor() { }

  select_cln(db: knex) {
    return db('cln');
  }

  select_yearbudget(db:knex){
    return db('yearbudget')
  }

  async getNamecln(db:knex,cln:any){
    let sql:any = `select namecln from cln as c
    where c.cln =  ${cln}`;
   let data:any = await db.raw(sql);
   return data[0];
  }
//REfer out by Cln
async getReferoutByCln(db: Knex, yearbudget: any,cln: any) {
  if (cln=='' || cln == 'null'){
    let data = await db('ovst as o')
    .innerJoin('cln as c', 'c.cln', 'o.cln')
    .innerJoin('orfro as r','o.vn','r.vn')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.ovstost','=','3')
    .count('o.vn as value')
    .select('c.namecln as name')
    .groupBy('o.cln');
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('cln as c', 'c.cln', 'o.cln')
    .innerJoin('orfro as r','o.vn','r.vn')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.cln',cln)
    .andWhere('o.ovstost','=','3')
    .count('o.vn as value')
    .select('c.namecln as name')
    .groupBy('o.cln');
    return data;
  }

}

//REfer in by Cln
async getReferinByCln(db: Knex, yearbudget: any,cln: any) {

  if (cln=='' || cln == 'null'){
    let data = await db('ovst as o')
    .innerJoin('cln as c', 'c.cln', 'o.cln')
    .innerJoin('orfri as r','o.vn','r.vn')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .count('o.vn as value')
    .select('c.namecln as name')
    .groupBy('o.cln');
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('cln as c', 'c.cln', 'o.cln')
    .innerJoin('orfri as r','o.vn','r.vn')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.cln',cln)
    .count('o.vn as value')
    .select('c.namecln as name')
    .groupBy('o.cln');
    return data;
  }

}

// REfer out by Age
async getReferoutByAge(db: Knex, yearbudget: any,cln: any) {
  if (cln =='' || cln == 'null'){
      let data = await db('ovst as o')
      .innerJoin('pt', 'pt.hn', 'o.hn')
      .innerJoin('orfro as r','o.vn','r.vn')
      .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
      .andWhere('o.ovstost','=','3')
      .count('o.vn as value')
      .select(db.raw('count(case pt.male when "1" then o.vn end) as male'))
      .select(db.raw('count(case pt.male when "2" then o.vn end) as female'))
      .select(db.raw('(case when timestampdiff(year,pt.brthdate,o.vstdttm) between 0 and 9 then "0-9"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 10 and 19 then "10-19"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 20 and 29 then "20-29"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 30 and 39 then "30-39"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 40 and 49 then "40-49"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 50 and 59 then "50-59"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 60 and 69 then "60-69"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 70 and 79 then "70-79"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 80 and 89 then "80-89"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 90 and 99 then "90-99"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) >= 100 then "99 up" else "NA" end) as name'))
      .groupBy('name');
    return data;
  }else{
      let data = await db('ovst as o')
      .innerJoin('pt', 'pt.hn', 'o.hn')
      .innerJoin('orfro as r','o.vn','r.vn')
      .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
      .andWhere('o.ovstost','=','3')
      .andWhere('o.cln',cln)
      .count('o.vn as value')
      .select(db.raw('count(case pt.male when "1" then o.vn end) as male'))
      .select(db.raw('count(case pt.male when "2" then o.vn end) as female'))
      .select(db.raw('(case when timestampdiff(year,pt.brthdate,o.vstdttm) between 0 and 9 then "0-9"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 10 and 19 then "10-19"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 20 and 29 then "20-29"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 30 and 39 then "30-39"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 40 and 49 then "40-49"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 50 and 59 then "50-59"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 60 and 69 then "60-69"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 70 and 79 then "70-79"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 80 and 89 then "80-89"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 90 and 99 then "90-99"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) >= 100 then "99 up" else "NA" end) as name'))
      .groupBy('name');
      return data;
  }

}

//Refer in by age
async getReferinByAge(db: Knex, yearbudget: any,cln: any) {
  if (cln =='' || cln == 'null'){
      let data = await db('ovst as o')
      .innerJoin('pt', 'pt.hn', 'o.hn')
      .innerJoin('orfri as r','o.vn','r.vn')
      .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
      .count('o.vn as value')
      .select(db.raw('count(case pt.male when "1" then o.vn end) as male'))
      .select(db.raw('count(case pt.male when "2" then o.vn end) as female'))
      .select(db.raw('(case when timestampdiff(year,pt.brthdate,o.vstdttm) between 0 and 9 then "0-9"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 10 and 19 then "10-19"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 20 and 29 then "20-29"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 30 and 39 then "30-39"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 40 and 49 then "40-49"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 50 and 59 then "50-59"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 60 and 69 then "60-69"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 70 and 79 then "70-79"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 80 and 89 then "80-89"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 90 and 99 then "90-99"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) >= 100 then "99 up" else "NA" end) as name'))
      .groupBy('name');
    return data;
  }else{
      let data = await db('ovst as o')
      .innerJoin('pt', 'pt.hn', 'o.hn')
      .innerJoin('orfri as r','o.vn','r.vn')
      .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
      .andWhere('o.cln',cln)
      .count('o.vn as value')
      .select(db.raw('count(case pt.male when "1" then o.vn end) as male'))
      .select(db.raw('count(case pt.male when "2" then o.vn end) as female'))
      .select(db.raw('(case when timestampdiff(year,pt.brthdate,o.vstdttm) between 0 and 9 then "0-9"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 10 and 19 then "10-19"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 20 and 29 then "20-29"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 30 and 39 then "30-39"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 40 and 49 then "40-49"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 50 and 59 then "50-59"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 60 and 69 then "60-69"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 70 and 79 then "70-79"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 80 and 89 then "80-89"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) between 90 and 99 then "90-99"' +
          ' when timestampdiff(year,pt.brthdate,o.vstdttm) >= 100 then "99 up" else "NA" end) as name'))
      .groupBy('name');
      return data;
  }

}
//Refer out by Gender
async getReferoutByGender(db: Knex, yearbudget: any,cln: any) {
  if(cln=='' || cln =='null'){
    let data = await db('ovst as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .innerJoin('pt', 'pt.hn', 'o.hn')
    .innerJoin('male', 'pt.male', 'male.male')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.ovstost','=','3')
    .count('o.vn as value')
    .select('male.namemale as name')
    .groupBy('pt.male');
  return data; 
  }else{
      let data = await db('ovst as o')
      .innerJoin('orfro as r','o.vn','r.vn')
      .innerJoin('pt', 'pt.hn', 'o.hn')
      .innerJoin('male', 'pt.male', 'male.male')
      .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
      .andWhere('o.ovstost','=','3')
      .andWhere('o.cln',cln)
      .count('o.vn as value')
      .select('male.namemale as name')
      .groupBy('pt.male');
      return data; 
  }
}


//Refer in by Gender
async getReferinByGender(db: Knex, yearbudget: any,cln: any) {
  if(cln=='' || cln =='null'){
    let data = await db('ovst as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pt', 'pt.hn', 'o.hn')
    .innerJoin('male', 'pt.male', 'male.male')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .count('o.vn as value')
    .select('male.namemale as name')
    .groupBy('pt.male');
  return data; 
  }else{
      let data = await db('ovst as o')
      .innerJoin('orfri as r','o.vn','r.vn')
      .innerJoin('pt', 'pt.hn', 'o.hn')
      .innerJoin('male', 'pt.male', 'male.male')
      .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
      .andWhere('o.cln',cln)
      .count('o.vn as value')
      .select('male.namemale as name')
      .groupBy('pt.male');
      return data; 
  }
}
//Refer out by Month
async getReferoutByMonth(db: Knex, yearbudget: any,cln: any) {
  if(cln =='' || cln =='null'){
    let data = await db('ovst as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.ovstost','=','3')
    .count('o.vn as value')
    .select(db.raw('month(o.vstdttm) as name'))
    .orderBy('o.vstdttm')
    .groupBy('name');
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.ovstost','=','3')
    .andWhere('o.cln',cln)
    .count('o.vn as value')
    .select(db.raw('month(o.vstdttm) as name'))
    .orderBy('o.vstdttm')
    .groupBy('name');
    return data;
  }

}

//Refer in by Month
async getReferinByMonth(db: Knex, yearbudget: any,cln: any) {
  if(cln =='' || cln =='null'){
    let data = await db('ovst as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .count('o.vn as value')
    .select(db.raw('month(o.vstdttm) as name'))
    .orderBy('o.vstdttm')
    .groupBy('name');
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.cln',cln)
    .count('o.vn as value')
    .select(db.raw('month(o.vstdttm) as name'))
    .orderBy('o.vstdttm')
    .groupBy('name');
    return data;
  }

}

//Refer out by Pttype
async getReferoutByPttype(db: Knex, yearbudget: any,cln: any) {
  if(cln=='' || cln=='null'){
    let data = await db('ovst as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .innerJoin('pttype as p', 'p.pttype', 'o.pttype')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.ovstost','=','3')
    .count('o.vn as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .innerJoin('pttype as p', 'p.pttype', 'o.pttype')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.ovstost','=','3')
    .andWhere('o.cln',cln)
    .count('o.vn as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }
}

//Refer in by Pttype
async getReferinByPttype(db: Knex, yearbudget: any,cln: any) {
  if(cln=='' || cln=='null'){
    let data = await db('ovst as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p', 'p.pttype', 'o.pttype')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .count('o.vn as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p', 'p.pttype', 'o.pttype')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ? ', [yearbudget]))
    .andWhere('o.cln',cln)
    .count('o.vn as value')
    .select('p.inscl as name')
    .orderBy('value', 'desc')
    .groupBy('p.inscl');
    return data;
  }
}

//Refer out by Incoth month
async getReferout_IncothByMonth(db: Knex, yearbudget: any,cln: any) {
  if(cln =='' || cln=='null'){
    let data = await db('ovst as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))   
    .andWhere('o.ovstost','=','3')       
    .select(db.raw('month(o.vstdttm) as name'))
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('name')
    .orderBy('o.vstdttm')
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))   
    .andWhere('o.ovstost','=','3')        
    .andWhere('o.cln',cln)
    .select(db.raw('month(o.vstdttm) as name'))
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('name')
    .orderBy('o.vstdttm')
    return data;
  }
}

//Refer in by Incoth month
async getReferin_IncothByMonth(db: Knex, yearbudget: any,cln: any) {
  if(cln =='' || cln=='null'){
    let data = await db('ovst as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))        
    .select(db.raw('month(o.vstdttm) as name'))
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('name')
    .orderBy('o.vstdttm')
    return data;
  }else{
    let data = await db('ovst as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))          
    .andWhere('o.cln',cln)
    .select(db.raw('month(o.vstdttm) as name'))
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('name')
    .orderBy('o.vstdttm')
    return data;
  }
}

//สรุปค่ารักษา + total visit
async getReferout_IncothByMonthPttype_Inscl(db: Knex, yearbudget: any,cln: any) {
  if(cln=='' || cln=='null'){
    let data = await db('ovst as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))   
    .andWhere('o.ovstost','=','3')         
    .select('p.inscl as name')
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }else {
    let data = await db('ovst as o')
    .innerJoin('orfro as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))  
    .andWhere('o.ovstost','=','3')           
    .andWhere('o.cln',cln)
    .select('p.inscl as name')
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }

}

//สรุปค่ารักษา + total visit
async getReferin_IncothByMonthPttype_Inscl(db: Knex, yearbudget: any,cln: any) {
  if(cln=='' || cln=='null'){
    let data = await db('ovst as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))            
    .select('p.inscl as name')
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }else {
    let data = await db('ovst as o')
    .innerJoin('orfri as r','o.vn','r.vn')
    .innerJoin('pttype as p','p.pttype','o.pttype')
    .innerJoin('incoth as i','i.vn','o.vn')
    .innerJoin('cln as c','c.cln','o.cln')
    .where(db.raw('if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1)= ? ', [yearbudget]))          
    .andWhere('o.cln',cln)
    .select('p.inscl as name')
    .countDistinct('o.vn as value')
    .sum('i.rcptamt as total')
    .groupBy('p.inscl')
    return data;
  }

}
//หาค่า max Refer out ในการแสดงกราฟ
async getMaxReferoutbyMonth(db: Knex, yearbudget: any,cln: any){
  if(cln =='' || cln =='null'){
    let sql:any = `SELECT name,max(value) as value,max(value) / length(max(value)) as strinter  from (
      SELECT month(o.vstdttm) as name,count(o.vn) as value from ovst as o 
      inner join orfro as r on o.vn = r.vn and o.ovstost = 3 
      where if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ?
      GROUP BY name
      order by o.vstdttm ) as x`
      let data:any = await db.raw(sql,[yearbudget]);
      return data[0];
  }else{
    let sql:any = `SELECT name,max(value) as value,max(value) / length(max(value)) as strinter  from (
      SELECT month(o.vstdttm) as name,count(o.vn) as value from ovst as o 
      inner join orfro as r on o.vn = r.vn and o.ovstost = 3
      where o.cln = ? and if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ?
      GROUP BY name
      order by o.vstdttm ) as x`
      let data:any = await db.raw(sql,[cln,yearbudget]);
      return data[0];
  }

}

//หาค่า max Refer in ในการแสดงกราฟ
async getMaxReferinbyMonth(db: Knex, yearbudget: any,cln: any){
  if(cln =='' || cln =='null'){
    let sql:any = `SELECT name,max(value) as value,max(value) / length(max(value)) as strinter  from (
      SELECT month(o.vstdttm) as name,count(o.vn) as value from ovst as o 
      inner join orfri as r on o.vn = r.vn 
      where if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ?
      GROUP BY name
      order by o.vstdttm ) as x`
      let data:any = await db.raw(sql,[yearbudget]);
      return data[0];
  }else{
    let sql:any = `SELECT name,max(value) as value,max(value) / length(max(value)) as strinter  from (
      SELECT month(o.vstdttm) as name,count(o.vn) as value from ovst as o 
      inner join orfri as r on o.vn = r.vn 
      where o.cln = ? and if(month(o.vstdttm) < 10,year(o.vstdttm),year(o.vstdttm)+1) = ?
      GROUP BY name
      order by o.vstdttm ) as x`
      let data:any = await db.raw(sql,[cln,yearbudget]);
      return data[0];
  }

}
//20 ลำดับโรค Refer out
async getDx20Referout(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln=='' || cln == 'null'){     
    let sql:any = `SELECT x.icd10 as code,i.icd10name as name,count(x.icd10) as total from ovst as o 
    INNER JOIN orfro as r on o.vn = r.vn and o.ovstost = 3
    INNER JOIN ovstdx as x on o.vn = x.vn and x.cnt = 1
    INNER JOIN icd101 as i on x.icd10 = i.icd10
    where date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY x.icd10 ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT x.icd10 as code,i.icd10name as name,count(x.icd10) as total from ovst as o 
    INNER JOIN orfro as r on o.vn = r.vn and o.ovstost = 3
    INNER JOIN ovstdx as x on o.vn = x.vn and x.cnt = 1
    INNER JOIN icd101 as i on x.icd10 = i.icd10
    where o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY x.icd10 ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับโรค Refer in
async getDx20Referin(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln=='' || cln == 'null'){     
    let sql:any = `SELECT x.icd10 as code,i.icd10name as name,count(x.icd10) as total from ovst as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN ovstdx as x on o.vn = x.vn and x.cnt = 1
    INNER JOIN icd101 as i on x.icd10 = i.icd10
    where date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY x.icd10 ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT x.icd10 as code,i.icd10name as name,count(x.icd10) as total from ovst as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN ovstdx as x on o.vn = x.vn and x.cnt = 1
    INNER JOIN icd101 as i on x.icd10 = i.icd10
    where o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY x.icd10 ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับคลินิก Refer out
async getCLN20Referout(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln=='' || cln == 'null'){     
    let sql:any = `SELECT o.cln as code,c.namecln as name,count(o.cln) as total from ovst as o 
    INNER JOIN orfro as r on o.vn = r.vn and o.ovstost = 3
    INNER JOIN cln as c on o.cln = c.cln
    where date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY o.cln ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT o.cln as code,c.namecln as name,count(o.cln) as total from ovst as o 
    INNER JOIN orfro as r on o.vn = r.vn and o.ovstost = 3
    INNER JOIN cln as c on o.cln = c.cln
    where o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY o.cln ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}
//20 ลำดับคลินิก Refer in
async getCLN20Referin(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln=='' || cln == 'null'){     
    let sql:any = `SELECT o.cln as code,c.namecln as name,count(o.cln) as total from ovst as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN cln as c on o.cln = c.cln
    where date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY o.cln ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT o.cln as code,c.namecln as name,count(o.cln) as total from ovst as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN cln as c on o.cln = c.cln
    where o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY o.cln ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับสาเหตุการ  Refer out
async getRfrcs20Referout(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln=='' || cln == 'null'){     
    let sql:any = `SELECT rfrcs as code ,namerfrcs as name ,sum(total) as total from (
      SELECT r.rfrcs,if(h.namerfrcs is null,'ไม่ระบุสาเหตุ',h.namerfrcs) as namerfrcs  ,count(r.rfrcs) as total from ovst as o 
      INNER JOIN orfro as r on o.vn = r.vn and o.ovstost = 3
      left JOIN rfrcs as h on r.rfrcs = h.rfrcs
      where date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrcs ORDER BY total desc  LIMIT 20 ) as x GROUP BY namerfrcs order by total desc`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT rfrcs as code,namerfrcs as name ,sum(total) as total from (
      SELECT r.rfrcs,if(h.namerfrcs is null,'ไม่ระบุสาเหตุ',h.namerfrcs) as namerfrcs  ,count(r.rfrcs) as total from ovst as o 
      INNER JOIN orfro as r on o.vn = r.vn and o.ovstost = 3
      left JOIN rfrcs as h on r.rfrcs = h.rfrcs
      where o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrcs ORDER BY total desc  LIMIT 20 ) as x GROUP BY namerfrcs order by total desc`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับสาเหตุการ  Refer in
async getRfrcs20Referin(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln=='' || cln == 'null'){     
    let sql:any = `SELECT rfrcs as code,namerfrcs as name ,sum(total) as total from (
      SELECT r.rfrcs,if(h.namerfrcs is null,'ไม่ระบุสาเหตุ',h.namerfrcs) as namerfrcs  ,count(r.rfrcs) as total from ovst as o 
      INNER JOIN orfri as r on o.vn = r.vn 
      left JOIN rfrcs as h on r.rfrcs = h.rfrcs
      where date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrcs ORDER BY total desc  LIMIT 20 ) as x GROUP BY namerfrcs order by total desc`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT rfrcs as code,namerfrcs as name ,sum(total) as total from (
      SELECT r.rfrcs,if(h.namerfrcs is null,'ไม่ระบุสาเหตุ',h.namerfrcs) as namerfrcs  ,count(r.rfrcs) as total from ovst as o 
      INNER JOIN orfri as r on o.vn = r.vn 
      left JOIN rfrcs as h on r.rfrcs = h.rfrcs
      where o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrcs ORDER BY total desc  LIMIT 20 ) as x GROUP BY namerfrcs order by total desc`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับสาเหตุการ  Refer out
async getHcode20Referout(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln=='' || cln == 'null'){     
    let sql:any = `SELECT r.rfrlct as code,h.namehosp as name,count(r.rfrlct) as total from ovst as o 
    INNER JOIN orfro as r on o.vn = r.vn and o.ovstost = 3
    INNER JOIN hospcode as h on r.rfrlct = h.off_id
    where date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrlct ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT r.rfrlct as code,h.namehosp as name,count(r.rfrlct) as total from ovst as o 
    INNER JOIN orfro as r on o.vn = r.vn and o.ovstost = 3
    INNER JOIN hospcode as h on r.rfrlct = h.off_id
    where o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrlct ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}

//20 ลำดับสาเหตุการ  Refer in
async getHcode20Referin(db: Knex, date_start:any,date_end: any,cln: any){
  if (cln=='' || cln == 'null'){     
    let sql:any = `SELECT r.rfrlct as code,h.namehosp as name,count(r.rfrlct) as total from ovst as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN hospcode as h on r.rfrlct = h.off_id
    where date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrlct ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }else{
    let sql:any = `SELECT r.rfrlct as code,h.namehosp as name,count(r.rfrlct) as total from ovst as o 
    INNER JOIN orfri as r on o.vn = r.vn 
    INNER JOIN hospcode as h on r.rfrlct = h.off_id
    where o.cln = '${cln}' and date(o.vstdttm) BETWEEN '${date_start}' and '${date_end}' GROUP BY r.rfrlct ORDER BY total desc  LIMIT 20`;
   let data:any = await db.raw(sql);
   return data[0];
  }

}
}