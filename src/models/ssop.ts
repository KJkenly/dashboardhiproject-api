import Knex, * as knex from 'knex';

export class SsopModel {

  constructor() { }

  select_idpm(db: knex) {
    return db('idpm');
  }

  select_cln(db: knex) {
    return db('cln');
  }

  select_yearbudget(db:knex){
    return db('yearbudget')
  }

  async selete_raw_code(db: knex,code:any){
    let sql:any = `SELECT u.id,u.code,u.fullname,d.code as codedpm,d.name as namedpm FROM users u 
    INNER JOIN l_dprtm d on d.code = u.code 
    WHERE u.code = '${code}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }


async aipn_all_statusflg(db: Knex,date_start:any,date_end: any){ 
      let sql:any = `select 
      count(x.invno) as countall,
      count(CASE WHEN x.tflag = '' THEN '' END) AS status_null,
      count(CASE WHEN x.tflag  = 'A' THEN 'A' END) AS status_a,
      count(CASE WHEN x.tflag  = 'E' THEN 'E' END) AS status_e,
      count(CASE WHEN x.tflag  = 'D' THEN 'D' END) AS status_d
 from (
      SELECT s.Invno as invno,date_format(b.DTtran,'%Y-%m-%d') as dttran,b.Hcode as hcode,
      s.HN as hn ,s.Pid as pid,b.Name as name,b.HMain as hmain,b.Amount as amount,
      b.Paid as paid,b.ClaimAmt as claimamt,if(b.Tflag is null,'',b.Tflag) as tflag
  from ssop_opservices as s 
    LEFT JOIN ssop_billtran as b on s.Invno = b.Invno
    where date(s.vstdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY s.Invno ) as x`;
     let data:any = await db.raw(sql);
     return data[0];
     
  }

  async ssop_statusflgN(db: Knex, date_start:any,date_end: any){
 
      let sql:any = `SELECT s.Invno as invno,date_format(b.DTtran,'%Y-%m-%d') as dttran_date,date_format(b.DTtran,'%H:%i:%s') as dttran_time,b.Hcode as hcode,
s.HN as hn ,
if(length(substr(s.Pid,5))= 9,concat(substr(s.Pid,5),'XXXXX'),'XXXXXXXXXXXXX') as pid,
b.Name as name,b.HMain as hmain,b.Amount as amount,
b.Paid as paid,b.ClaimAmt as claimamt,if(b.Tflag is null,'',Tflag) as tflag
 from ssop_opservices as s 
LEFT JOIN ssop_billtran as b on s.Invno = b.Invno
where b.Tflag is null and date(s.vstdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY s.Invno`;
     let data:any = await db.raw(sql);
     return data[0];


  }

  async ssop_statusflgA(db: Knex, date_start:any,date_end: any){
 
    let sql:any = `SELECT s.Invno as invno,date_format(b.DTtran,'%Y-%m-%d') as dttran_date,date_format(b.DTtran,'%H:%i:%s') as dttran_time,b.Hcode as hcode,
    s.HN as hn ,
    if(length(substr(s.Pid,5))= 9,concat(substr(s.Pid,5),'XXXXX'),'XXXXXXXXXXXXX') as pid,
    b.Name as name,b.HMain as hmain,b.Amount as amount,
    b.Paid as paid,b.ClaimAmt as claimamt,if(b.Tflag is null,'',Tflag) as tflag
     from ssop_opservices as s 
    LEFT JOIN ssop_billtran as b on s.Invno = b.Invno
    where b.Tflag = 'A' and date(s.vstdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY s.Invno`;
         let data:any = await db.raw(sql);
         return data[0];

  }

  async ssop_statusflgE(db: Knex,date_start:any,date_end: any){
 
    let sql:any = `SELECT s.Invno as invno,date_format(b.DTtran,'%Y-%m-%d') as dttran_date,date_format(b.DTtran,'%H:%i:%s') as dttran_time,b.Hcode as hcode,
    s.HN as hn ,
    if(length(substr(s.Pid,5))= 9,concat(substr(s.Pid,5),'XXXXX'),'XXXXXXXXXXXXX') as pid,
    b.Name as name,b.HMain as hmain,b.Amount as amount,
    b.Paid as paid,b.ClaimAmt as claimamt,if(b.Tflag is null,'',Tflag) as tflag
     from ssop_opservices as s 
    LEFT JOIN ssop_billtran as b on s.Invno = b.Invno
    where b.Tflag = 'E' and date(s.vstdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY s.Invno`;
         let data:any = await db.raw(sql);
         return data[0];

  }
  async ssop_statusflgD(db: Knex, date_start:any,date_end: any){
 
    let sql:any = `SELECT s.Invno as invno,date_format(b.DTtran,'%Y-%m-%d') as dttran_date,date_format(b.DTtran,'%H:%i:%s') as dttran_time,b.Hcode as hcode,
    s.HN as hn ,
    if(length(substr(s.Pid,5))= 9,concat(substr(s.Pid,5),'XXXXX'),'XXXXXXXXXXXXX') as pid,
    b.Name as name,b.HMain as hmain,b.Amount as amount,
    b.Paid as paid,b.ClaimAmt as claimamt,if(b.Tflag is null,'',Tflag) as tflag
     from ssop_opservices as s 
    LEFT JOIN ssop_billtran as b on s.Invno = b.Invno
    where b.Tflag = 'D' and date(s.vstdate) BETWEEN '${date_start}' and '${date_end}' GROUP BY s.Invno`;
         let data:any = await db.raw(sql);
         return data[0];
  }
}