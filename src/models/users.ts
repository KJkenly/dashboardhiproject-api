import Knex, * as knex from 'knex';

export class UsersModel {

  constructor() { }

  select(db: knex) {
    return db('users');
  }

  insert(db: knex,datas: any){
    return db('users').insert(datas);
  }

  update(db: Knex,datas: any,id: any){
    return db('users').update(datas).where('id',id);
  }

  delete(db: Knex,id: any){
    return db('users').delete().where('id',id);
  }

  select_code(db: knex,code: any) {
    return db('users').where('code',code);
  }

  // async selete_raw_code(db: knex,code:any){
  //   let sql:any = `SELECT u.id,u.code,u.fullname,d.code as codedpm,d.name as namedpm FROM users u 
  //   INNER JOIN l_dprtm d on d.code = u.code 
  //   WHERE u.code = '${code}'`;
  //   let data:any = await db.raw(sql);
  //   return data[0];
  // }
  async selete_raw_cid(db: knex,cid:any){
    let sql:any = `SELECT u.* FROM users u 
    WHERE u.cid = '${cid}'`;
    let data:any = await db.raw(sql);
    return data[0];
  }
  async select_dprtm(db: knex) {
    return db('l_dprtm');
  }
}