import * as crypto from 'crypto'

import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { LoginModel } from '../models/login'

export default async function login(fastify: FastifyInstance) {

  const db: any = fastify.hicm
  const loginModel = new LoginModel()

  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body
    const username = body.username
    const password = body.password

    try {
      // const encPassword = crypto.createHash('md5').update(password).digest('hex')
      const rs: any = await loginModel.login(db, username, password)
      //ถ้ามากกว่า 0 ทำงาน
      if (rs.length > 0) {
        const user: any = rs[0]
        const payload: any = {
          userId: user.id,
          is_active: user.is_active,
          fullname: user.fullname,
          cid: user.cid
        }

        const token = fastify.jwt.sign(payload)
        reply.send({ token })
      } else {
        reply.status(401).send({ ok: false, message: 'Login failed' })
      }


    } catch (error:any) {
      reply.status(500).send({ message: error.message })
    }
  })

  

}
