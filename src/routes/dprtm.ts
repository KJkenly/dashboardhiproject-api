import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { DprtmModel } from '../models/dprtm'

export default async function test(fastify: FastifyInstance) {

  const fromImport = new DprtmModel()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'DPRTM Model' })
  })


  fastify.get('/select', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    try {
      const rs: any = await fromImport.select(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })


  fastify.post('/insert', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const datas: any = req.body;
    try {
      const rs: any = await fromImport.insert(db,datas)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.put('/update/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const datas: any = req.body;
    const id:any = req.params.id
    try {
      const rs: any = await fromImport.update(db,datas,id)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.delete('/delete/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const id:any = req.params.id

    try {
      const rs: any = await fromImport.delete(db,id)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_code', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.mysql2;
    const req: any = request;
    const code: any = req.body.code;
    try {
      const rs: any = await fromImport.select_code(db,code)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

}
