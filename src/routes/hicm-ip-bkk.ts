import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { StmIpBkkModel } from '../models/hicm-ip-bkk'

import { log } from 'console'

const fromImport = new StmIpBkkModel()

export default async function test(fastify: FastifyInstance) {

  const db = fastify.hicm;
  
  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'STM IP BKK Model' })
  })
  
  fastify.post('/select_debt_account', async (request: FastifyRequest, reply: FastifyReply) => {
    
    try {
      const rs: any = await fromImport.select_debt_account(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_debt_account_hi', async (request: FastifyRequest, reply: FastifyReply) => {
    const dbhi = fastify.hi;
    try {
      const rs: any = await fromImport.select_debt_account_hi(dbhi)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/stm_countan', async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const info:any = req.body;
    console.log(info);
    
    try {
        let rs:any = await fromImport.stm_countan(db,info.accType,info.startDate,info.endDate); 
  
        if (!rs.length) {
            let datas = {
                "ok": false,
                "text": "การบันทึกข้อมูลผิดพลาด",
                "rows": rs
            };
            reply.code(200).send({ statusCode: 204,results: datas })
          } else {
            let datas = {
                "ok": true,
                "text": "การบันทึกข้อมูลสำเร็จ",
                "rows": rs
            };
            reply.code(200).send({ statusCode: 204, results: datas})
          }
    } catch (error:any) {
      req.log.error(error);
      reply.code(500).send({
          ok: false,
          text: "การอ่านข้อมูลเกิดความผิดพลาด",
          error: error.message
      });     
    } 

});

  fastify.post('/stm_ip_bkk', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const info:any = req.body;
    console.log(db);
    try {
        let rs:any = await fromImport.select_stm_ip_bkk(db,info.repno); 
        if (!rs.length) {
            let datas = {
                "ok": false,
                "text": "การอ่านข้อมูลผิดพลาด",
                "rows": rs
            };
            reply.code(200).send({ statusCode: 204,results: datas })
          } else {
            let datas = {
                "ok": true,
                "text": "การอ่านข้อมูลสำเร็จ",
                "rows": rs
            };
            reply.code(200).send({ statusCode: 204, results: datas})
          }
    } catch (error:any) {
        req.log.error(error);
        reply.code(500).send({
            ok: false,
            text: "การอ่านข้อมูลเกิดความผิดพลาด",
            error: error.message
        });        
    } 
}); 

fastify.post('/stm_ip_bkk_stmno', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const req:any = request;
  const info:any = req.body;
  console.log(db);
  try {
      let rs:any = await fromImport.select_stm_ip_bkk(db,info.stm_no); 
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การอ่านข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การอ่านข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204, results: datas})
        }
  } catch (error:any) {
      req.log.error(error);
      reply.code(500).send({
          ok: false,
          text: "การอ่านข้อมูลเกิดความผิดพลาด",
          error: error.message
      });        
  } 
}); 
fastify.post('/ipbkknotnull_stm', async (request: FastifyRequest, reply: FastifyReply) => {
  const req:any = request;
  const info:any = req.body;
  console.log(info);
  
  try {
      let rs:any = await fromImport.ipbkknotnull_stm(db,info.accType,info.startDate,info.endDate); 

      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        } else {
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ statusCode: 204,results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });     
  } 

});
fastify.post('/ipbkkaccnotnull_stm', async (request: FastifyRequest, reply: FastifyReply) => {
const req:any = request;
const info:any = req.body;
console.log(info);

try {
    let rs:any = await fromImport.ipbkkaccnotnull_stm(db,info.accType,info.startDate,info.endDate); 

    if (!rs.length) {
        let datas = {
            "ok": false,
            "text": "การบันทึกข้อมูลผิดพลาด",
            "rows": rs
        };
        reply.code(200).send({ statusCode: 204,results: datas })
      } else {
        let datas = {
            "ok": true,
            "text": "การบันทึกข้อมูลสำเร็จ",
            "rows": rs
        };
        reply.code(200).send({ statusCode: 204,results: datas })
      }
} catch (error:any) {
  req.log.error(error);
  reply.code(500).send({
      ok: false,
      text: "การอ่านข้อมูลเกิดความผิดพลาด",
      error: error.message
  });     
} 

});
fastify.post('/ipbkknull', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.ipbkknull(db,info.accType,info.startDate,info.endDate); 
      
            if (!rs.length) {
                let datas = {
                    "ok": false,
                    "text": "การบันทึกข้อมูลผิดพลาด",
                    "rows": rs
                };
                reply.code(200).send({ results: datas })
              } else {

                for (const i of rs) {

                    if (i.dchdate) {
                    const date = new Date(i.dchdate);
                    const year = date.getUTCFullYear();
                    const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                    const day = String(date.getUTCDate()).padStart(2, '0');
                    const formattedDate = `${year}-${month}-${day}`;
                    i.dchdate=formattedDate;
                    }         
                    if (i.admitdate) {
                        const date = new Date(i.admitdate);
                        const year = date.getUTCFullYear();
                        const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                        const day = String(date.getUTCDate()).padStart(2, '0');
                        const formattedDate = `${year}-${month}-${day}`;
                        i.admitdate=formattedDate;
                     }                 
                }

                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ results: datas })
              }
        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });    
        } 
    
    });

    fastify.post('/ipbkkaccnotnull', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.ipbkkaccnotnull(db,info.accType,info.startDate,info.endDate); 
      
            if (!rs.length) {
                let datas = {
                    "ok": false,
                    "text": "การบันทึกข้อมูลผิดพลาด",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              } else {
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204, results: datas})
              }
        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });     
        } 
    
    });

    fastify.post('/ipbkkaccnull', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.ipbkkaccnull(db,info.accType,info.startDate,info.endDate); 
      
            if (!rs.length) {
                let datas = {
                    "ok": false,
                    "text": "การบันทึกข้อมูลผิดพลาด",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              } else {
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              }
        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });     
        } 
    
    });
  
    fastify.post('/ipbkknotnull', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.ipbkknotnull(db,info.accType,info.startDate,info.endDate); 
      
            if (!rs.length) {
                let datas = {
                    "ok": false,
                    "text": "การบันทึกข้อมูลผิดพลาด",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              } else {
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              }
        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });       
        } 
    
    });
      
    fastify.post('/ipbkkaccbydate', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.ipbkkaccbydate(db,info.accType,info.startDate,info.endDate); 
      
            if (!rs.length) {
             
                let datas = {
                    "ok": false,
                    "text": "การบันทึกข้อมูลผิดพลาด",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              } else {
              
                for (const i of rs) {

                    if (i.dchdate) {
                    const date = new Date(i.dchdate);
                    const year = date.getUTCFullYear();
                    const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                    const day = String(date.getUTCDate()).padStart(2, '0');
                    const formattedDate = `${year}-${month}-${day}`;
                    i.dchdate=formattedDate;
                    }                    
                }
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })
              }
        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });     
        } 
    
    });

  
    fastify.post('/stm_ip_bkk_sum', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);
        
        try {
            let rs:any = await fromImport.stm_ip_bkk_sum(db,info.repno); 
    
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })

        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });     
        } 
    
    });

    fastify.post('/stm_ip_bkk_sum_stmno', async (request: FastifyRequest, reply: FastifyReply) => {
      const req:any = request;
      const info:any = req.body;
      console.log(info);
      
      try {
          let rs:any = await fromImport.stm_ip_bkk_sum(db,info.stm_no); 
  
              let datas = {
                  "ok": true,
                  "text": "การบันทึกข้อมูลสำเร็จ",
                  "rows": rs
              };
              reply.code(200).send({ statusCode: 204,results: datas })

      } catch (error:any) {
        req.log.error(error);
        reply.code(500).send({
            ok: false,
            text: "การอ่านข้อมูลเกิดความผิดพลาด",
            error: error.message
        });     
      } 
  
  });

    fastify.post('/stm_ip_bkk_detail', async (request: FastifyRequest, reply: FastifyReply) => {
        const req:any = request;
        const info:any = req.body;
        console.log(info);   
        try {
            let rs:any = await fromImport.stm_ip_bkk_detail(db,info.repno); 
    
                let datas = {
                    "ok": true,
                    "text": "การบันทึกข้อมูลสำเร็จ",
                    "rows": rs
                };
                reply.code(200).send({ statusCode: 204,results: datas })

        } catch (error:any) {
          req.log.error(error);
          reply.code(500).send({
              ok: false,
              text: "การอ่านข้อมูลเกิดความผิดพลาด",
              error: error.message
          });     
        } 
    
    });
    fastify.post('/toperrorcode', async (request: FastifyRequest, reply: FastifyReply) => {
      const req:any = request;
      const info:any = req.body;
      console.log(info);
      
      try {
          let rs:any = await fromImport.toperrorcode(db,info.accType,info.startDate,info.endDate); 
    
          if (!rs.length) {
              let datas = {
                  "ok": false,
                  "text": "การบันทึกข้อมูลผิดพลาด",
                  "rows": rs
              };
              reply.code(200).send({ results: datas })
            } else {

              for (const i of rs) {

                  if (i.dchdate) {
                  const date = new Date(i.dchdate);
                  const year = date.getUTCFullYear();
                  const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                  const day = String(date.getUTCDate()).padStart(2, '0');
                  const formattedDate = `${year}-${month}-${day}`;
                  i.dchdate=formattedDate;
                  }         
                  if (i.admitdate) {
                      const date = new Date(i.admitdate);
                      const year = date.getUTCFullYear();
                      const month = String(date.getUTCMonth() + 1).padStart(2, '0');
                      const day = String(date.getUTCDate()).padStart(2, '0');
                      const formattedDate = `${year}-${month}-${day}`;
                      i.admitdate=formattedDate;
                   }                 
              }

              let datas = {
                  "ok": true,
                  "text": "การบันทึกข้อมูลสำเร็จ",
                  "rows": rs
              };
              reply.code(200).send({ results: datas })
            }
      } catch (error:any) {
        req.log.error(error);
        reply.code(500).send({
            ok: false,
            text: "การอ่านข้อมูลเกิดความผิดพลาด",
            error: error.message
        });    
      } 
  
  });       
}
