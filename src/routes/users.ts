import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { UsersModel } from '../models/users'

export default async function test(fastify: FastifyInstance) {
  const db = fastify.hicm;
  const fromImport = new UsersModel()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Users Model' })
  })


  fastify.get('/select',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const rs: any = await fromImport.select(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/insert',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {

    const req: any = request;
    const datas: any = req.body;
    try {
      const rs: any = await fromImport.insert(db,datas)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.put('/update/:id',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const datas: any = req.body;
    const id:any = req.params.id
    try {
      const rs: any = await fromImport.update(db,datas,id)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.delete('/delete/:id', { preValidation: [fastify.authenticate] },async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id:any = req.params.id

    try {
      const rs: any = await fromImport.delete(db,id)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_code',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const code: any = req.body.code;
    try {
      const rs: any = await fromImport.select_code(db,code)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })


  // fastify.post('/select_raw_code',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  //   const db: any = fastify.mysql2;
  //   const req: any = request;
  //   const code: any = req.body.code;
  //   try {
  //     const rs: any = await fromImport.selete_raw_code(db,code)

  //     reply.send(rs)
  //   } catch (error:any) {
  //     reply.send({ message: error.message })
  //   }
  // })


  fastify.post('/select_raw_cid',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const cid: any = req.body.cid;
    try {
      const rs: any = await fromImport.selete_raw_cid(db,cid)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })
  
  // fastify.get('/select_dprtm',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  //   try {
  //     const rs: any = await fromImport.select(db)

  //     reply.send(rs)
  //   } catch (error:any) {
  //     reply.send({ message: error.message })
  //   }
  // })
}
