import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { BookClaimModel } from '../models/book-claim'
import { log } from 'console'

export default async function test(fastify: FastifyInstance) {

  const fromImport = new BookClaimModel()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'BookClaim Model' })
  })

  fastify.post('/bookclaim_all_statusflg',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      const rs: any = await fromImport.bookclaim_all_statusflg(db,info.startDate,info.endDate)
      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
});

  fastify.post('/selete_raw_bookclaim',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req:any = request;
    const info:any = req.body;    
    try {
      let rs:any = await fromImport.selete_raw_bookclaim(db,info.startDate,info.endDate); 
  
      if (!rs.length) {
          let datas = {
              "ok": false,
              "text": "การบันทึกข้อมูลผิดพลาด",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        } else {
 
          let datas = {
              "ok": true,
              "text": "การบันทึกข้อมูลสำเร็จ",
              "rows": rs
          };
          reply.code(200).send({ results: datas })
        }
  } catch (error:any) {
    req.log.error(error);
    reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
    });    
  }  
  });



}
