import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { DashboardModel } from '../models/dashboard'
import { log } from 'console'

export default async function test(fastify: FastifyInstance) {

  const fromImport = new DashboardModel()

  fastify.get('/', (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: 'Dashboard Model' })
  })

  fastify.get('/select_cln', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    try {
      const rs: any = await fromImport.select_cln(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })


  fastify.get('/select_yearbudget', async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hicm;
    try {
      const rs: any = await fromImport.select_yearbudget(db)

      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/select_raw_code',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req: any = request;
    const code: any = req.body.code;
    try {
      const rs: any = await fromImport.selete_raw_code(db,code)
      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

  fastify.post('/service_opd_all',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
      const db: any = fastify.hi;
      const req: any = request.body;
      let yearbudget = req.yearbudget;
      let codecln = req.cln;

      if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
          yearbudget = new Date().getFullYear();
      }

      const start_date = (yearbudget - 1) + '-10-01';
      const end_date = yearbudget + '-09-30';
      const cln = codecln;
      console.log(end_date);
      
      try {
        const rs: any = await fromImport.service_opd_all(db, start_date, end_date,cln)
        reply.send(rs)
      } catch (error:any) {
        reply.send({ message: error.message })
      }
  })

  fastify.post('/service_opd_null',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req: any = request.body;
    let yearbudget = req.yearbudget;
    let codecln = req.cln;

    if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
        yearbudget = new Date().getFullYear();
    }

    const start_date = (yearbudget - 1) + '-10-01';
    const end_date = yearbudget + '-09-30';
    const cln = codecln;
    console.log(end_date);
    
    try {
      const rs: any = await fromImport.service_opd_null(db, start_date, end_date,cln)
      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
})

  fastify.post('/service_opd_home',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const db: any = fastify.hi;
    const req: any = request.body;
    let yearbudget = req.yearbudget;
    let codecln = req.cln;

    if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
        yearbudget = new Date().getFullYear();
    }

    const start_date = (yearbudget - 1) + '-10-01';
    const end_date = yearbudget + '-09-30';
    const cln = codecln;
    console.log(end_date);
    
    try {
      const rs: any = await fromImport.service_opd_home(db, start_date, end_date,cln)
      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
})
  
fastify.post('/service_opd_dead',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.cln;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const cln = codecln;
  console.log(end_date);
  
  try {
    const rs: any = await fromImport.service_opd_dead(db, start_date, end_date,cln)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
})

fastify.post('/service_opd_refer',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.cln;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const cln = codecln;
  console.log(end_date);
  
  try {
    const rs: any = await fromImport.service_opd_refer(db, start_date, end_date,cln)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
})

fastify.post('/service_opd_refer_in',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.cln;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const cln = codecln;
  console.log(end_date);
  
  try {
    const rs: any = await fromImport.service_opd_refer_in(db, start_date, end_date,cln)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
})

fastify.post('/service_opd_admit',  { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req: any = request.body;
  let yearbudget = req.yearbudget;
  let codecln = req.cln;

  if (yearbudget == null || yearbudget == undefined || yearbudget == '') {
      yearbudget = new Date().getFullYear();
  }

  const start_date = (yearbudget - 1) + '-10-01';
  const end_date = yearbudget + '-09-30';
  const cln = codecln;
  console.log(end_date);
  
  try {
    const rs: any = await fromImport.service_opd_admit(db, start_date, end_date,cln)
    reply.send(rs)
  } catch (error:any) {
    reply.send({ message: error.message })
  }
})


fastify.post('/getOPsummary', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
  const db: any = fastify.hi;
  const req :any = request.body;
  const yearbudget :any = req.yearbudget;
  const cln = req.cln;
  let info :any = {};
  try {
      const clinic = await fromImport.getOpVisitByClinic(db, yearbudget,cln);
      if(clinic.length > 0){

      }
      const gender = await fromImport.getOpVisitByGender(db, yearbudget,cln);
      const age = await fromImport.getOpVisitByAge(db, yearbudget,cln);
      const month = await fromImport.getOpVisitByMonth(db, yearbudget,cln);
      const pttype = await fromImport.getOpVisitByPttype(db, yearbudget,cln);
      const sincothmm = await fromImport.getVisit_IncothByMonth(db, yearbudget,cln);
      const sincothinscl = await fromImport.getVisit_IncothByMonthPttype_Inscl(db,yearbudget,cln);
      const maxopvisit_m = await fromImport.getMaxOpVisitbyMonth(db,yearbudget,cln);
      info = {
          ok: true,
          text: "รายงานข้อมูลผู้ป่วยนอก",
          clinic: clinic,
          gender: gender,
          age: age,
          month: month,
          pttype: pttype,
          sincothmm:sincothmm,
          sincothinscl:sincothinscl,
          maxopvisit_m:maxopvisit_m
      }
      reply.code(200).send(info);

  } catch (error :any) {
      req.log.error(error);
      reply.code(500).send({
          ok: false,
          text: "การอ่านข้อมูลเกิดความผิดพลาด",
          error: error.message
      });
  }
});
}
