import Knex, * as knex from 'knex';

export class ProductivityModel {

  constructor() { }

  select(db: knex) {
    return db('productivity');
  }

  insert(db: knex,datas: any){
    return db('productivity').insert(datas);
  }

  update(db: Knex,datas: any,id: any){
    return db('productivity').update(datas).where('id',id);
  }

  delete(db: Knex,id: any){
    return db('productivity').delete().where('id',id);
  }

  select_id(db: knex,id: any) {
    return db('productivity').where('id',id);
  }

  async select_date(db: Knex,datestart: any,dateend: any){
    let sql:any = `select l.name,p.* from productivity as p
    INNER JOIN l_dprtm as l on p.codewd = l.code where p.vstdate BETWEEN '${datestart}' and '${dateend}' order by p.vstdate,p.codewd,p.wer`;
    let data:any = await db.raw(sql);
    return data[0];
    //return db('productivity').whereBetween('vstdate',[datestart,dateend])
  }

  async select_date_code(db: Knex,datestart: any,dateend: any,code: any){
    let sql:any = `select l.name,p.* from productivity as p
    INNER JOIN l_dprtm as l on p.codewd = l.code where p.codewd = '${code}' and p.vstdate BETWEEN '${datestart}' and '${dateend}' order by p.vstdate,p.codewd,p.wer`;
    let data:any = await db.raw(sql);
    return data[0];
  }

  select_dprtm(db: knex,id: any){
    return db('l_dprtm').where('code',id);
  }
 //รายงานอัตรากำลังต่อวัน/เดือน/ปี
  async select_pd_year(db: knex,year: any,code: any){
    let sql:any =
    `SELECT day(vstdate) as cday,
    cast(sum(case month(vstdate) when 1 then pd_day end) as decimal(10,2)) as m1, 
    cast(sum(case month(vstdate) when 2 then pd_day end) as decimal(10,2)) as m2, 
    cast(sum(case month(vstdate) when 3 then pd_day end) as decimal(10,2)) as m3, 
    cast(sum(case month(vstdate) when 4 then pd_day end) as decimal(10,2)) as m4, 
    cast(sum(case month(vstdate) when 5 then pd_day end) as decimal(10,2)) as m5, 
    cast(sum(case month(vstdate) when 6 then pd_day end) as decimal(10,2)) as m6, 
    cast(sum(case month(vstdate) when 7 then pd_day end) as decimal(10,2)) as m7, 
    cast(sum(case month(vstdate) when 8 then pd_day end) as decimal(10,2)) as m8, 
    cast(sum(case month(vstdate) when 9 then pd_day end) as decimal(10,2)) as m9, 
    cast(sum(case month(vstdate) when 10 then pd_day end) as decimal(10,2)) as m10, 
    cast(sum(case month(vstdate) when 11 then pd_day end) as decimal(10,2)) as m11, 
    cast(sum(case month(vstdate) when 12 then pd_day end) as decimal(10,2)) as m12 
     from productivity where codewd = '${code}' and year(vstdate) = '${year}' GROUP BY cday 
     union 
    select 'total', 
    cast(sum(case month(vstdate) when 1 then pd_day end) as decimal(10,2)) as m1,
    cast(sum(case month(vstdate) when 2 then pd_day end) as decimal(10,2)) as m2,
    cast(sum(case month(vstdate) when 3 then pd_day end) as decimal(10,2)) as m3,
    cast(sum(case month(vstdate) when 4 then pd_day end) as decimal(10,2)) as m4,
    cast(sum(case month(vstdate) when 5 then pd_day end) as decimal(10,2)) as m5,
    cast(sum(case month(vstdate) when 6 then pd_day end) as decimal(10,2)) as m6,
    cast(sum(case month(vstdate) when 7 then pd_day end) as decimal(10,2)) as m7,
    cast(sum(case month(vstdate) when 8 then pd_day end) as decimal(10,2)) as m8,
    cast(sum(case month(vstdate) when 9 then pd_day end) as decimal(10,2)) as m9,
    cast(sum(case month(vstdate) when 10 then pd_day end) as decimal(10,2)) as m10,
    cast(sum(case month(vstdate) when 11 then pd_day end) as decimal(10,2)) as m11,
    cast(sum(case month(vstdate) when 12 then pd_day end) as decimal(10,2)) as m12 
    from productivity where codewd = '${code}' and year(vstdate) = '${year}'`
    let data:any = await db.raw(sql);
    return data[0];
  }
//รายงานอัตรากำลังเฉลี่ยรายเดือน/รายปี/หอผู้ป่วย
async select_pd_month(db: knex,year: any){
  let sql: any = 
  `select d.name,
  round(sum(case month(vstdate) when 1 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m1,
  round(sum(case month(vstdate) when 2 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m2,
  round(sum(case month(vstdate) when 3 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m3,
  round(sum(case month(vstdate) when 4 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m4,
  round(sum(case month(vstdate) when 5 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m5,
  round(sum(case month(vstdate) when 6 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m6,
  round(sum(case month(vstdate) when 7 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m7,
  round(sum(case month(vstdate) when 8 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m8,
  round(sum(case month(vstdate) when 9 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m9,
  round(sum(case month(vstdate) when 10 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m10,
  round(sum(case month(vstdate) when 11 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m11,
  round(sum(case month(vstdate) when 12 then pd_day end)/ day(LAST_DAY(vstdate)),2) as m12,
  round(round(sum(pd_day)/12,2)/12,2) as ctotal 
  from productivity  
  INNER JOIN l_dprtm as d on productivity.codewd = d.code 
  where year(vstdate) = '${year}' group by d.name` 
  let data:any = await db.raw(sql);
  return data[0];

}

async select_pd_pttype(db: knex,datestart: any,dateend: any){
  let sql:any = 
  `select d.name,
  sum(total_icu) as t_icu,
  sum(total_ci) as t_ci, 
  sum(total_mi) as t_mi, 
  sum(total_cl) as t_cl, 
  sum(total_si) as t_si 
   from productivity  as p
  INNER JOIN l_dprtm as d on p.codewd = d.code
   where p.vstdate between '${datestart}' and '${dateend}' group by d.name`
   let data: any = await db.raw(sql);
   return data[0];
}

}
